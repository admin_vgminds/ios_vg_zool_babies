//
//  FileManager.swift
//  Kids App
//
//  Created by Vikram Rao on 05/01/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation

class AssetManager {
    
    static func exists(httpUrl:String) -> Bool {
        if let url = localUrl(httpUrl: httpUrl) {
            let fileManager = FileManager()
          
            return fileManager.fileExists(atPath: url.path)
        }
        return false
    }
    
    static func assetDirectory() -> String {
        let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.applicationSupportDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let applicationDirectory:String = "\(path[0])/\(Bundle.main.bundleIdentifier!)"
        if !FileManager.default.fileExists(atPath: applicationDirectory) {
            do {
                try FileManager.default.createDirectory(atPath: applicationDirectory, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print("Could not create application directory - \(error.localizedDescription)")
            }
        }
        return applicationDirectory
    }
    
    static func assetUrlFor(_ fileName: String) -> URL {
        return URL(fileURLWithPath: assetDirectory().appendingFormat("/\(fileName)"))
    }
    
    static func clearAssetVideos() {
        do {
            let subPaths = try FileManager.default.contentsOfDirectory(atPath: assetDirectory())
            let assetsDirectoryUrl = URL(fileURLWithPath: assetDirectory(), isDirectory: true)
            for path in subPaths {
                let pathUrl = URL(fileURLWithPath: path, relativeTo: assetsDirectoryUrl)
                if !["jpg","jpeg","png"].contains(pathUrl.pathExtension) {
                    try FileManager.default.removeItem(at: pathUrl)
                }
            }
        } catch {
            print("Could not clear asset videos - \(error.localizedDescription)")
        }
    }
    
    static func clearAssetsDir() {
        do {
            try FileManager.default.removeItem(atPath: assetDirectory())
        } catch {
            print("Could not clear asset directory - \(error.localizedDescription)")
        }
    }
    
    static func localUrl(httpUrl:String) -> URL? {
        if let url = URL(string: httpUrl) {
            let fileName = url.lastPathComponent
            return assetUrlFor(fileName)
        }
        return nil
    }
    
}
