//
//  DataLoader.swift
//  Kids App
//
//  Created by Vikram Rao on 04/01/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation

class DataLoader {
    
    static let sharedInstance:DataLoader = DataLoader()

    var mainConfig:Dictionary<String,Any>!
    var versionInfo:Dictionary<String,Any>!
    var videoInfo:Dictionary<String,Any>!
    var videos:[Dictionary<String,Any>]!
    var adsInfo:Dictionary<String,Any>!
    var otherAppsInfo:Dictionary<String,Any>!
//    var oldVideoFileMap:Dictionary<String,Any>!
    
    private init() {
        load()
    }
    
    fileprivate func copyBundledThumbnail(_ thumbnailUrl: String) -> Bool {
        let thumbNailFileName = URL(string: thumbnailUrl)?.lastPathComponent
        let bundleUrl = Bundle.main.resourceURL
        let bundleThumbNailUrl = bundleUrl?.appendingPathComponent(thumbNailFileName!)
        do {
            try FileManager.default.copyItem(at: bundleThumbNailUrl!, to: AssetManager.assetUrlFor(thumbNailFileName!))
            return true
        } catch {
            print("Could not copy thumbnail from bundle to application directory - \(error.localizedDescription)")
        }
        return false
    }
    
    fileprivate func santizeVideosData() {
        var validVideos:[Dictionary<String,Any>] = []
        for index in 0..<videos.count {
            var video = videos[index] as Dictionary<String,Any>
            let keys = video.keys
            if keys.contains("VideoUrl") {
                if keys.contains("AnimatedThumbnail") {
                    if (video["AnimatedThumbnail"] as! Bool) {
                        validVideos.append(video)
                    } else if keys.contains("ThumbnailUrl") {
                        validVideos.append(video)
                    }
                } else if keys.contains("ThumbnailUrl") {
                    video["AnimatedThumbnail"] = false
                    validVideos.append(video)
                }
            }
        }
        videos = validVideos
    }
    
    fileprivate func upateVideoDownloadStatus() {
        for index in 0..<videos.count {
            let videoUrl = videos[index]["VideoUrl"] as! String
            videos[index]["downloaded"] = AssetManager.exists(httpUrl: videoUrl)
        }
    }
    
    fileprivate func loadJsonFiles() {
        mainConfig = JSONDataLoader.loadData("main-config", "MainConfig")
        versionInfo = JSONDataLoader.loadData("version-info", "Version")
        videoInfo = JSONDataLoader.loadData("video-info", "Video")
        otherAppsInfo = JSONDataLoader.loadData("other-apps-info", "OtherApps")
        adsInfo = JSONDataLoader.loadData("ads-info", "Ads")
//        oldVideoFileMap = JSONDataLoader.loadData("old-video-file-map", "OldVideoFileMap")
        videos = videoInfo["VideosList"] as! [Dictionary<String,Any>]
    }
    
    private func load() {
        loadJsonFiles()
//        Migrator.migrateOldFormatVideos(oldVideoFileMap)
        santizeVideosData()
        upateVideoDownloadStatus()
        copyBundledThumbnails()
    }
    
    func copyBundledThumbnails() {
        for index in 0..<videos.count {
            let animatedThumbnail = videos[index]["AnimatedThumbnail"] as! Bool
            if animatedThumbnail {
                videos[index]["thumbnail-downloaded"] = false
                continue
            }
            let thumbnailUrl = videos[index]["ThumbnailUrl"] as! String
            var thumbNailExists = AssetManager.exists(httpUrl: thumbnailUrl)
            if !thumbNailExists {
                if copyBundledThumbnail(thumbnailUrl) {
                    thumbNailExists = true
                }
            }
            print("Thumbnail: \(thumbnailUrl) - exists? \(thumbNailExists)")
            videos[index]["thumbnail-downloaded"] = thumbNailExists
        }
    }
    
    func refresh(_ complete:@escaping (()->Void)) {
        JSONRemoteDataLoader.loadRemote("Version") {versionInfo in
            if versionInfo.count == 0 || JSONDataLoader.isRhsVersionNewerOrSame(lhs:versionInfo, rhs:self.versionInfo) {
                complete()
                return
            }
            UserDefaults.standard.set(versionInfo, forKey: "version-info")
            JSONRemoteDataLoader.loadRemote("Video", complete: {videoInfo in
                if videoInfo.count == 0 {
                    complete()
                    return
                }
                if !JSONDataLoader.isRhsVersionNewerOrSame(lhs:videoInfo, rhs:self.videoInfo) {
                    AssetManager.clearAssetsDir()
                    UserDefaults.standard.set(videoInfo, forKey: "video-info")
                }
                JSONRemoteDataLoader.loadRemote("Ads", complete: { ads in
                    if ads.count == 0 {
                        complete()
                        return
                    }
                    if !JSONDataLoader.isRhsVersionNewerOrSame(lhs:ads, rhs:self.adsInfo) {
                        UserDefaults.standard.set(ads, forKey: "ads-info")
                    }
                    JSONRemoteDataLoader.loadRemote("OtherApps", complete: { otherAppsInfo in
                        if otherAppsInfo.count == 0 {
                            complete()
                            return
                        }
                        if !JSONDataLoader.isRhsVersionNewerOrSame(lhs:otherAppsInfo, rhs:self.otherAppsInfo) {
                            UserDefaults.standard.set(otherAppsInfo, forKey: "other-apps-info")
                        }
                        self.load()
                        complete()
                    })
                })
            })
        }
    }
    
    fileprivate func downloadThumbnailIcon(_ index: Int, complete:@escaping (()->Void)) {
        let animatedThumbnail = videos[index]["AnimatedThumbnail"] as! Bool
        let thumbNailCached = videos[index]["thumbnail-downloaded"] as! Bool
        if animatedThumbnail || thumbNailCached {
            complete()
            return
        }
        let thumbnailUrl = videos[index]["ThumbnailUrl"] as! String
        FileDownloadQueue.sharedInstance.download(url: thumbnailUrl, callback: { (_ percentComplete:Int) in
            if percentComplete == 100 || percentComplete == -1 {
                self.videos[index]["thumbnail-downloaded"] = (percentComplete == 100)
                complete()
            }
        })
    }
    
    func cacheIcons(complete:@escaping (()->Void)) {
        var pending = videos.count
        for index in 0..<videos.count {
            downloadThumbnailIcon(index, complete: {
                pending -= 1
                if pending == 0 {
                    self.cacheOtherAppsIcon {
                        complete()
                    }
                }
            })
        }
    }
    
    fileprivate func downloadOtherAppIcon(_ index: Int, complete:@escaping (()->Void)) {
        let otherApps = otherAppsInfo["OtherApps"]! as! [Dictionary<String,Any>]
        let thumbnailUrl = otherApps[index]["ThumbnailLink"] as! String
        let thumbNailExists = AssetManager.exists(httpUrl: thumbnailUrl)
        if thumbNailExists {
            complete()
            return
        }
        FileDownloadQueue.sharedInstance.download(url: thumbnailUrl, callback: { (_ percentComplete:Int) in
            if percentComplete == 100 || percentComplete == -1 {
                complete()
            }
        })
    }
    
    func cacheOtherAppsIcon(complete:@escaping (()->Void)) {
        let otherApps = self.otherAppsInfo["OtherApps"]! as! [Dictionary<String,Any>]
        var pending = otherApps.count
        for index in 0..<otherApps.count {
            downloadOtherAppIcon(index, complete: {
                pending -= 1
                if pending == 0 {
                    complete()
                }
            })
        }
    }
    
    
    
}
