//
//  JSONRemoteDataLoader.swift
//  Kids Phonics
//
//  Created by Vikram Rao on 26/02/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation

class JSONRemoteDataLoader {
    
    static let configuration = URLSessionConfiguration.default
    static var session:URLSession!
    static var task:URLSessionDataTask!
    
    static func loadRemote(_ fileName:String, complete:@escaping ((_ data:Dictionary<String,Any>)->Void)) {
        let url = "https://1876484730.rsc.cdn77.org/vg/rhymes/ios/zoolbabies/config/\(fileName).json"
        configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
        configuration.urlCache = nil
        session = URLSession(configuration: configuration)
        task = session.dataTask(with: URL(string: url)!) { (data, response, error) in
            if error != nil {
                print("Error Occurred")
                DispatchQueue.main.async {
                    complete([:])
                }
                return
            }
            do {
                let rawArr = try JSONSerialization.jsonObject(with: data!, options: [])
                let arr = rawArr as! Dictionary<String, Any>
                DispatchQueue.main.async {
                    complete(arr)
                }
            } catch {
                DispatchQueue.main.async {
                    complete([:])
                }
            }
        }
        task.resume()
    }
}
