//
//  GameViewController.swift
//  Kids App
//
//  Created by Vikram Rao on 02/01/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import Appodeal
import Crashlytics

class GameViewController: UIViewController, ThumbnailCardDelegate, AppodealBannerDelegate {
  
    override func viewDidLoad() {
        super.viewDidLoad()
        if let view = self.view as! SKView? {
            let scene = GameScene(size: view.bounds.size)
            scene.scaleMode = .resizeFill
            scene.thumbNailDelegate = self
            view.presentScene(scene)
            view.showsFPS = false
            view.showsNodeCount = false
//            let button = UIButton(type: .roundedRect)
//            button.frame = CGRect(x: 20, y: 50, width: 100, height: 30)
//            button.setTitle("Crash", for: [])
//            button.addTarget(self, action: #selector(self.crashButtonTapped(_:)), for: .touchUpInside)
//            view.addSubview(button)
            
            
        }
    }
        @IBAction func crashButtonTapped(_ sender: AnyObject) {
            Crashlytics.sharedInstance().crash()
        }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func thumbNailCardDidTap(videoIndex:Int) {
        let videoVC = storyboard?.instantiateViewController(withIdentifier: "video-player") as! VideoPlayerController
        videoVC.videos = DataLoader.sharedInstance.videos
        videoVC.videoIndex = videoIndex
        AudioPlayer.stop()
        AudioPlayer.playSound("ButtonClick")
        navigationController?.pushViewController(videoVC, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AudioPlayer.playMusic("BGMusic")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "VIEW_WILL_APPEAR"), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        print("Memory Warning")
        super.didReceiveMemoryWarning()
    }
}
