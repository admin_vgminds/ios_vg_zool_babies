//
//  AppDelegate.swift
//  Kids App
//
//  Created by Vikram Rao on 02/01/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import UIKit
import AVFoundation
import Firebase
import OneSignal
import Appodeal
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    let locale = Locale.current
    var window: UIWindow?
    var backgroundSessionCompletionHandler: (() -> Void)?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UIApplication.shared.isIdleTimerDisabled = false
        Fabric.with([Crashlytics.self])
        UserDefaults.standard.register(defaults: ["settings-sounds" : true, "settings-music" : true, "settings-autoplay" : true])
        registerAudioPlaybackType()
        FirebaseApp.configure()
        initOneSignal(launchOptions: launchOptions)
        initAppodeal()
        return true
    }
    
    fileprivate func registerAudioPlaybackType() {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSessionCategoryPlayback)
        } catch {
            print("Setting category to AVAudioSessionCategoryPlayback failed.")
        }
    }
    
    fileprivate func initAppodeal() {
        #if DEBUG
        Appodeal.setTestingEnabled(false)
        #endif
        
        let adTypes:AppodealAdType = [.interstitial]
        Appodeal.initialize(withApiKey: "1a455eb7061016a5fe70dab8c64ff4ef4fe7c5aaf8ef1b53", types:  adTypes)
        let country_Code  = locale.regionCode
        let country = locale.localizedString(forRegionCode: country_Code!)
        if country != nil
        {
            let CountryName=country!
            print("The Country Name \(CountryName)")
            let Names = ["Albania","Andorra","Armenia","Austria","Azerbaijan","Belarus","Belgium","Bosnia & Herzegovia","Bulgaria","Ukraine","Croatia","Cyprus","Czechia", "Denmark","Estonia","Finland","France","Georgia","Germany","Greece","Hungary","Iceland","Ireland","Italy","Kazakhstan","KosovoLatvia","Liechtenstein","Lithuania","Luxembourg","Macedonia","Malta","Moldova","Monaco","Montenegro","Netherlands","Norway","Poland","Portugal","Romania","Russia","San Marino","Serbia","Slovakia","Slovenia","Spain","Sweden","Switzerland","Turkey","United Kingdom", "Vatican City"];
            
            if Names.contains(CountryName)
            {
                Appodeal.setChildDirectedTreatment(true)
            }
            
            
        }
        
        
    }
    
    func initOneSignal(launchOptions: [UIApplicationLaunchOptionsKey: Any]?) {
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: "85ddd8be-5a6f-412c-9d5f-058d53034ad9",
                                        handleNotificationAction: nil,
                                        settings: onesignalInitSettings)
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
    }
    
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        backgroundSessionCompletionHandler = completionHandler
    }
}

