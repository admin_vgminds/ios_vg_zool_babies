//
//  AudioPlayer.swift
//  Unjumble-Swift
//
//  Created by Vikram Rao on 19/12/17.
//  Copyright © 2017 Vikram Rao. All rights reserved.
//

import Foundation
import AVFoundation

class _AudioPlayer {
    var player: AVAudioPlayer?
    var audioFile: String?
    
    init() {
        do {
            try AVAudioSession.sharedInstance().setActive(true)
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func play(_ file:String, volume:Float = 1.0, loop:Bool) {
        if let player = self.player {
            if (audioFile ?? "") == file && player.isPlaying && loop {
                print("Playing In Progress - Ignoring - \(file) - current [\(audioFile ?? "Unknown") - \(player.isPlaying)]")
                return
            }
            if player.isPlaying {
                print("Stopping existing player")
                player.stop()
            }
        }
        print("Playing - \(file)")
        audioFile = file
        guard let url = Bundle.main.url(forResource: file, withExtension: "mp3") else {
            print("Audio file not found")
            return
        }
        do {
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.wav.rawValue)
            guard let player = player else {
                print("Could not create player")
                return
            }
            player.numberOfLoops = (loop ? -1 : 0)
            player.play()
            player.volume = volume
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func stop() {
        guard let player = player else {
            print("There is no player to stop")
            return
        }
        player.stop()
        print("Player stopped")
    }
}

class AudioPlayer {
    
    static var soundPlayer = _AudioPlayer()
    static var musicPlayer = _AudioPlayer()
    
    fileprivate static func audioAllowed() -> Bool {
        return true
    }
    
    public static func playSound(_ file:String, volume:Float = 1.0) {
        if audioAllowed() && UserDefaults.standard.bool(forKey: "settings-sounds"){
            soundPlayer.play(file, volume: volume, loop: false)
        }
    }
    
    public static func playMusic(_ file:String, volume:Float = 1.0) {
        if audioAllowed() && UserDefaults.standard.bool(forKey: "settings-music") {
            musicPlayer.play(file, volume: volume, loop: true)
        }
    }
    
    public static func stop() {
        musicPlayer.stop()
    }
}
