
//
//  ThumbnailCard.swift
//  Kids App
//
//  Created by Vikram Rao on 04/01/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import UIKit
import SpriteKit

class ThumbnailCard: SKSpriteNode {
    
    private let colors = [UIColor(rgbColorCodeRed: 90, green: 214, blue: 236, alpha: 1.0), UIColor(rgbColorCodeRed: 255, green: 168, blue: 87, alpha: 1.0), UIColor(rgbColorCodeRed: 255, green: 95, blue: 138, alpha: 1.0)]

    var videoIndex:Int!

    private var freeCard:SKSpriteNode?
    private var thumbnailAction:ThumbnailActionButton!
    private var downloaded:Bool = false
    private var downloading:Bool = false
    private var thumbNailSize:CGSize!
    private var thumbNailImageSize:CGSize!
    private var halfThumbNailWidth:CGFloat!, halfThumbNailHeight:CGFloat!
    
    init(videoIndex:Int, texture: SKTexture?, thumbNailSize:CGSize, thumbNailImageSize:CGSize) {
        let color = colors[videoIndex % colors.count]
        let thumbNailWithShadowSize = CGSize(width: thumbNailSize.width+Thumbnail.ShadowSize*2, height: thumbNailSize.height+Thumbnail.ShadowSize*2)
        halfThumbNailWidth = thumbNailSize.width * Metrics.half
        halfThumbNailHeight = thumbNailSize.height * Metrics.half
        
        super.init(texture: texture, color: color, size: thumbNailWithShadowSize)
        
        self.thumbNailSize = thumbNailSize
        self.thumbNailImageSize = thumbNailImageSize
        self.videoIndex = videoIndex
        
        addThumbnailBackground()
        addPreviewImage()
        addInfoSection()
        
        NotificationCenter.default.addObserver(self, selector: #selector(assetCleared), name: NSNotification.Name(rawValue: "ASSETS_CLEARED"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateIfDownloading), name: NSNotification.Name(rawValue: "VIEW_WILL_APPEAR"), object: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //UI Rendering
    
    fileprivate func addPreviewImage() {
        let videoInfo = DataLoader.sharedInstance.videos![videoIndex]
        var thumbNail:SKSpriteNode
        if videoInfo["AnimatedThumbnail"] as! Bool {
            thumbNail = AnimatedThumbnailImage(pos:videoIndex, color: color, thumbNailImageSize: thumbNailImageSize, videoInfo:videoInfo, thumbNailSize:size)
        } else {
            thumbNail = StaticThumbnailImage(pos:videoIndex, color: color, thumbNailImageSize: thumbNailImageSize, videoInfo:videoInfo, thumbNailSize:size)
        }
        addChild(thumbNail)
    }
    
    fileprivate func addThumbnailBackground() {
        let thumbNailBg = SKShapeNode(rectOf: thumbNailSize, cornerRadius: 5.0)
        thumbNailBg.strokeColor = self.color
        thumbNailBg.fillColor = self.color
        thumbNailBg.position = CGPoint(x: 0, y: 0)
        addChild(thumbNailBg)
    }
    
    fileprivate func addFreeRibbon() {
        let freeCard = SKSpriteNode(texture: SKTexture(imageNamed:"thumbnail-free-bg"), size: CGSize(width: Thumbnail.FreeLabelWidth, height: Thumbnail.FreeLabelWidth))
        freeCard.position = CGPoint(x: halfThumbNailWidth, y: halfThumbNailHeight)
        freeCard.animateWobble()
        
        let freeMessage = SKLabelNode(text: "Free")
        freeMessage.fontSize = 11 * Metrics.factor
        freeMessage.fontColor = UIColor.white
        freeMessage.fontName = "ProximaNova-Bold"
        freeMessage.position = CGPoint(x:0,y:0)
        freeMessage.horizontalAlignmentMode = .center
        freeMessage.verticalAlignmentMode = .center
        freeCard.addChild(freeMessage)
        freeCard.isHidden = downloaded
        self.freeCard = freeCard
        addChild(freeCard)
    }
    
    fileprivate func addVideoName(_ videoInfo: [String : Any], _ labelSectionWidth: CGFloat, _ halfThumbNailInfoSectionHeight: CGFloat, _ halfInfoSectionBottomPadding: CGFloat) {
        let text = videoInfo["VideoName"] as! String
        let videoName = SKLabelNode(text: text)
        let font = UIFont(name: "ProximaNova-Semibold", size: 14 * Metrics.factor)?.fontThatFits(text: text, width: labelSectionWidth - 10)
        videoName.fontSize = (font?.pointSize)!
        videoName.fontName = "ProximaNova-Semibold"
        videoName.fontColor = UIColor.black
        videoName.horizontalAlignmentMode = .center
        videoName.verticalAlignmentMode = .center
        videoName.position = CGPoint(x: -thumbNailSize.width * Metrics.half + labelSectionWidth * Metrics.half, y: -halfThumbNailHeight + halfThumbNailInfoSectionHeight + halfInfoSectionBottomPadding)
        addChild(videoName)
    }
    
    fileprivate func addVideoNameSectionBackground(_ labelSectionWidth: CGFloat, _ infoSectionHeight: CGFloat, _ halfThumbNailInfoSectionHeight: CGFloat, _ halfInfoSectionBottomPadding: CGFloat) {
        let labelSectionBackground = SKSpriteNode(color: UIColor(rgbColorCodeRed: 252, green: 228, blue: 65, alpha: 1.0), size: CGSize(width: labelSectionWidth, height: infoSectionHeight))
        labelSectionBackground.position = CGPoint(x: -thumbNailSize.width * Metrics.half + labelSectionWidth * Metrics.half, y: -halfThumbNailHeight + halfThumbNailInfoSectionHeight + halfInfoSectionBottomPadding)
        addChild(labelSectionBackground)
    }
    
    func addInfoSection() {
        let videoInfo = DataLoader.sharedInstance.videos![videoIndex]
        self.downloaded = videoInfo["downloaded"] as! Bool
        
        let halfThumbNailInfoSectionHeight:CGFloat = Thumbnail.InfoSectionHeight * Metrics.half
        let halfInfoSectionBottomPadding:CGFloat = Thumbnail.InfoSectionBottomPadding * Metrics.half
        let infoSectionHeight:CGFloat = Thumbnail.InfoSectionHeight - Thumbnail.InfoSectionBottomPadding
        let labelSectionWidth:CGFloat = thumbNailSize.width - Thumbnail.ButtonWidth
        let halfThumbNailButtonWidth:CGFloat = Thumbnail.ButtonWidth * Metrics.half
        
        addVideoNameSectionBackground(labelSectionWidth, infoSectionHeight, halfThumbNailInfoSectionHeight, halfInfoSectionBottomPadding)
        addVideoName(videoInfo, labelSectionWidth, halfThumbNailInfoSectionHeight, halfInfoSectionBottomPadding)
        
        thumbnailAction = ThumbnailActionButton(size:CGSize(width: Thumbnail.ButtonWidth, height: infoSectionHeight), downloaded:downloaded)
        thumbnailAction.position = CGPoint(x: halfThumbNailWidth - halfThumbNailButtonWidth, y: -halfThumbNailHeight + halfThumbNailInfoSectionHeight + halfInfoSectionBottomPadding)
        addChild(thumbnailAction)
        
        addFreeRibbon()
    }
    
    //UI Rendering END
    
    fileprivate func onVideoDownloaded() {
        self.downloading = false
        self.downloaded = true
        thumbnailAction.showVideoAsDownloaded()
        if let freeCard = self.freeCard {
            freeCard.isHidden = true
        }
    }
    
    fileprivate func showDownloading() {
        downloading = true
        thumbnailAction.showDownloadingArc()
    }
    
    @objc func updateIfDownloading() {
        let videoInfo = DataLoader.sharedInstance.videos![videoIndex]
        let videoUrl = videoInfo["VideoUrl"] as! String
        if FileDownloadQueue.sharedInstance.isDownloading(url: videoUrl) {
            if !downloading {
                showDownloading()
            }
            FileDownloadQueue.sharedInstance.attach(url: videoUrl, callback: { progress in
                self.onDownloadProgress(progress)
            })
        } else if (DataLoader.sharedInstance.videos[self.videoIndex]["downloaded"] as! Bool) {
            onVideoDownloaded()
        } else {
            self.downloading = false
            thumbnailAction.showVideoAsNotDownloaded()
        }
    }
    
    func downloadFile(url:String) {
        if !Reachability.isConnectedToNetwork() {
            Toast.show(message:"No Internet Connection", scene:self.scene)
            AudioPlayer.playSound("NoInternetConnection")
            return;
        }
        if self.downloading {
            return
        }
        AudioPlayer.playSound("DownloadingVideo")
        Toast.show(message:"Downloading...", scene:self.scene)
        showDownloading()
        FileDownloadQueue.sharedInstance.download(url: url) { (progress:Int) in
            self.onDownloadProgress(progress)
        }
    }
    
    fileprivate func onDownloadProgress(_ progress:Int) {
        if progress > 0 {
            thumbnailAction.updateProgressArc(progress)
        }
        if progress == 100 || progress == -1 {
            if progress == 100 {
                DataLoader.sharedInstance.videos[self.videoIndex]["downloaded"] = true
                self.downloaded = true
            }
            self.downloading = false
            thumbnailAction.onDownloadEnd(progress)
            onVideoDownloaded()
            return
        }
    }
    
    @objc fileprivate func assetCleared() {
        if self.downloading || !self.downloaded {
            return
        }
        self.downloaded = false
        DataLoader.sharedInstance.videos[self.videoIndex]["downloaded"] = false
        thumbnailAction.showVideoAsNotDownloaded()
        freeCard?.isHidden = false
    }
}
