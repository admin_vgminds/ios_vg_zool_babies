//
//  ImagePreview.swift
//  Kids Phonics
//
//  Created by Vikram Rao on 18/03/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation
import SpriteKit

class ThumbnailImage: SKSpriteNode {
    func getTexture(name:String) -> SKTexture? {
        if Bundle.main.path(forResource: name, ofType: "png") != nil {
            return SKTexture(imageNamed: name)
        }
        return nil
    }
}

class AnimatedThumbnailImage: ThumbnailImage {
    convenience init(pos:Int, color: UIColor, thumbNailImageSize: CGSize, videoInfo:[String:Any], thumbNailSize:CGSize) {
        self.init(color: color, size: thumbNailImageSize)
        let halfThumbNailHeight:CGFloat = thumbNailSize.height * Metrics.half
        
        let thumbNail = SKSpriteNode(color: color, size: thumbNailImageSize)
        thumbNail.isUserInteractionEnabled = false
        
        self.position = CGPoint(x: 0, y: -halfThumbNailHeight + Thumbnail.InfoSectionHeight + (thumbNailSize.height - thumbNailImageSize.height - Thumbnail.InfoSectionHeight) * Metrics.half + thumbNailImageSize.height * Metrics.half)
        thumbNail.position = CGPoint(x: 0, y: 0)
        addChild(thumbNail)
        
        if let texture = getTexture(name: "anim_\(pos+1)_1") {
            var textures = [texture]
            if let texture2 = getTexture(name: "anim_\(pos+1)_2") {
                textures.append(texture2)
            }
            if let texture3 = getTexture(name: "anim_\(pos+1)_3") {
                textures.append(texture3)
            }
            thumbNail.texture = texture
            thumbNail.run(SKAction.repeatForever(SKAction.animate(with: textures, timePerFrame: 0.5)))
        }
    }
}

class StaticThumbnailImage: ThumbnailImage {
    convenience init(pos:Int, color: UIColor, thumbNailImageSize: CGSize, videoInfo:[String:Any], thumbNailSize:CGSize) {
        self.init(color: color, size: thumbNailImageSize)
        
        let halfThumbNailHeight:CGFloat = thumbNailSize.height * Metrics.half
        
        let thumbNail = SKSpriteNode(color: color, size: thumbNailImageSize)
        thumbNail.isUserInteractionEnabled = false
        
        let thumbnailHolder = SKCropNode()
        thumbnailHolder.position = CGPoint(x: 0, y: 0)
        self.position = CGPoint(x: 0, y: -halfThumbNailHeight + Thumbnail.InfoSectionHeight + (thumbNailSize.height - Thumbnail.InfoSectionHeight) * Metrics.half)
        addChild(thumbnailHolder)
        
        let maskNode = SKShapeNode(rectOf: thumbNailImageSize, cornerRadius: 3)
        maskNode.strokeColor = color
        maskNode.fillColor = color
        maskNode.lineWidth = 1.0
        thumbnailHolder.maskNode = maskNode
        
        thumbNail.position = CGPoint(x: 0, y: 0)
        thumbnailHolder.addChild(thumbNail)
        
        if videoInfo["thumbnail-downloaded"] as! Bool {
            if let thumbNailUrl = AssetManager.localUrl(httpUrl: videoInfo["ThumbnailUrl"] as! String) {
                if let thumbNailImage = UIImage(contentsOfFile: thumbNailUrl.path) {
                    thumbNail.texture = SKTexture(image: thumbNailImage)
                    thumbNail.aspectFillToSize(fillSize: thumbNailImageSize)
                }
            }
        }
    }
}
