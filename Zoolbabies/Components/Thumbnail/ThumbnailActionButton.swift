//
//  ThumbnailActionButton.swift
//  Kids Phonics
//
//  Created by Vikram Rao on 18/03/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation
import SpriteKit

class ThumbnailActionButton: SKSpriteNode {
    
    private var downloadProgressIndicator:SKLabelNode!
    private var playIcon:SKSpriteNode!
    private var downloadProgressArc:SKShapeNode!
    private var downloadProgressArcRadius:CGFloat = 0
    
    convenience init(size:CGSize, downloaded:Bool) {
        self.init(color: UIColor(rgbColorCodeRed: 255, green: 241, blue: 147, alpha: 1.0), size: size)
        addPlayIcon(downloaded)
        addDownloadProgressIndicator(downloaded)
    }
    
    fileprivate func addPlayIcon(_ downloaded:Bool) {
        let playIcon = SKSpriteNode(texture: SKTexture(imageNamed: (downloaded ? "thumbnail-play" : "thumbnail-download")), size: CGSize(width: Thumbnail.PlayIconWidth, height: Thumbnail.PlayIconWidth))
        playIcon.position = CGPoint(x: 0, y: 0)
        addChild(playIcon)
        self.playIcon = playIcon
    }
    
    fileprivate func addDownloadProgressIndicator(_ downloaded:Bool) {
        let downloadProgressIndicator = SKLabelNode(text: (downloaded ? "100%" : "0%"))
        downloadProgressIndicator.fontSize = 10 * Metrics.factor
        downloadProgressIndicator.fontName = "ProximaNova-Semibold"
        downloadProgressIndicator.fontColor = UIColor.black
        downloadProgressIndicator.horizontalAlignmentMode = .center
        downloadProgressIndicator.verticalAlignmentMode = .center
        downloadProgressIndicator.position = CGPoint(x: 0, y: 0)
        downloadProgressIndicator.isHidden = true
        addChild(downloadProgressIndicator)
        self.downloadProgressIndicator = downloadProgressIndicator
        
        let downloadProgressArcRadius:CGFloat = 12 * Metrics.factor
        let downloadProgressArcInitialPath = UIBezierPath(arcCenter: CGPoint(x:0, y:0), radius: downloadProgressArcRadius, startAngle: 0.degreesToRadians, endAngle: 0.degreesToRadians, clockwise: true)
        let downloadProgressArc = SKShapeNode(path: downloadProgressArcInitialPath.cgPath)
        downloadProgressArc.position = CGPoint(x: 0, y: 0)
        downloadProgressArc.strokeColor = UIColor(rgbColorCodeRed: 51, green: 180, blue: 0, alpha: 1.0)
        downloadProgressArc.fillColor = UIColor.clear
        downloadProgressArc.lineWidth = 1
        downloadProgressArc.lineCap = CGLineCap(rawValue: 1)!
        downloadProgressArc.isAntialiased = true
        downloadProgressArc.isHidden = true
        addChild(downloadProgressArc)
        self.downloadProgressArc = downloadProgressArc
        self.downloadProgressArcRadius = downloadProgressArcRadius
    }
    
    func showVideoAsNotDownloaded() {
        playIcon.texture = SKTexture(imageNamed:"thumbnail-download")
        playIcon.isHidden = false
        downloadProgressArc.isHidden = true
        downloadProgressIndicator.isHidden = true
    }
    
    func updateProgressArc(_ progress: Int) {
        let path = UIBezierPath(arcCenter: CGPoint(x:0, y:0), radius: self.downloadProgressArcRadius, startAngle: 0.degreesToRadians, endAngle: Int(360 - (Double(progress)/Double(100) * 360)).degreesToRadians, clockwise: false)
        self.downloadProgressArc.path = path.cgPath
        downloadProgressIndicator.text = "\(progress)%"
    }
    
    func onDownloadEnd(_ progress: Int) {
        if progress == 100 {
            showVideoAsDownloaded()
            Toast.show(message:"Downloaded Successfully", scene:self.scene)
        } else {
            showVideoAsNotDownloaded()
            Toast.show(message:"Could not download video", scene:self.scene)
        }
    }
    
    func showDownloadingArc() {
        playIcon.isHidden = true
        downloadProgressArc.isHidden = false
        let path = UIBezierPath(arcCenter: CGPoint(x:0, y:0), radius: self.downloadProgressArcRadius, startAngle: 0.degreesToRadians, endAngle: 0.degreesToRadians, clockwise: true)
        downloadProgressArc.path = path.cgPath
        downloadProgressIndicator.isHidden = false
        downloadProgressIndicator.text = "0%"
    }
    
    func showVideoAsDownloaded() {
        playIcon.texture = SKTexture(imageNamed:"thumbnail-play")
        playIcon.isHidden = false
        if let downloadProgressArc = self.downloadProgressArc {
            downloadProgressArc.isHidden = true
        }
        if let downloadProgressIndicator = self.downloadProgressIndicator {
            downloadProgressIndicator.isHidden = true
        }
    }
}
