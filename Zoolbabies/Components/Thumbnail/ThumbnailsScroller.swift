//
//  ThumbnailsScroller.swift
//  Kids App
//
//  Created by Vikram Rao on 06/01/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit

class ThumbnailsScroller: NSObject, UIScrollViewDelegate {
    
    weak private var thumbnailsScene:GameScene!
    weak private var view:UIView!
    weak private var thumbNailDelegate:ThumbnailCardDelegate!
    
    private var thumbnailsScrollView:UIScrollView!
    private var contentView:SKSpriteNode!
    private var contentSize:CGSize!
    private var imageLoadingQueue = DispatchQueue(label: "imagequeue")
    private var halfThumbNailWidth:CGFloat!, halfThumbNailHeight:CGFloat!
    private var thumbNailSize:CGSize!, thumbNailImageSize:CGSize!

    func make(thumbNailDelegate:ThumbnailCardDelegate, scene:GameScene) {
        self.thumbNailDelegate = thumbNailDelegate
        thumbnailsScene = scene
        self.view = scene.view
        
        //Create video thumbnails
        let videos = DataLoader.sharedInstance.videos!
        let videosCount:CGFloat = CGFloat(videos.count)

        thumbNailImageSize = getThumbNailImageSize()
        thumbNailSize = getThumbNailSize(thumbNailImageSize: thumbNailImageSize)
        print("thumbNailSize -> \(thumbNailSize)")
        halfThumbNailWidth = thumbNailSize.width * Metrics.half
        halfThumbNailHeight = thumbNailSize.height * Metrics.half
        
        contentSize = CGSize(width: (videosCount * thumbNailSize.width) + (Thumbnail.HorizontalPadding * (videosCount - 1)) + Thumbnail.ContentEdgePadding * 2, height: thumbNailSize.height)
        contentView = SKSpriteNode(color: UIColor.clear, size: contentSize)
        let contentViewPosition = CGPoint(x: 0, y: view.bounds.midY-Metrics.navBarHeight*Metrics.half)
        contentView.position = contentViewPosition
        contentView.anchorPoint = CGPoint(x:0, y:Metrics.half)
        scene.addChild(contentView)
        
        //ScrollView Elements (Thumbnails)
        for pos in 0..<Int(videosCount) {
            addThumbNailCard(pos)
        }
        
        addScrollView(thumbNailSize, contentSize)
        
        NotificationCenter.default.addObserver(self, selector: #selector(orientationChanged), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(assetCleared), name: NSNotification.Name(rawValue: "ASSETS_CLEARED"), object: nil)
    }
    
    @objc func assetCleared() {
        Toast.show(message: "All downloaded videos erased", scene: self.thumbnailsScene)
    }
    
    func getThumbNailImageSize() -> CGSize {
        let thumbNailOverallHeight:CGFloat = view.bounds.size.height - Metrics.navBarHeight - Thumbnail.VerticalMargin * 2 + Thumbnail.ShadowSize * 2
        let thumbNailImageHeight:CGFloat = thumbNailOverallHeight - Thumbnail.InfoSectionHeight - Thumbnail.ImagePadding * 2 - Thumbnail.ShadowSize * 2
        let thumbNailImageWidth:CGFloat = thumbNailImageHeight * CGFloat(176.5) / CGFloat(145)
        let thumbNailImageSize = CGSize(width: thumbNailImageWidth, height: thumbNailImageHeight)
        print("thumbNailImageSize -> \(thumbNailImageSize)")
        return thumbNailImageSize
    }
    
    fileprivate func addThumbNailCard(_ pos: Int) {
        let thumbNailCard = ThumbnailCard(videoIndex:pos, texture: SKTexture(imageNamed: "tile"), thumbNailSize: thumbNailSize, thumbNailImageSize:thumbNailImageSize)
        let x:CGFloat = Thumbnail.ContentEdgePadding + Thumbnail.HorizontalPadding * CGFloat(pos) + thumbNailSize.width * CGFloat(pos) + halfThumbNailWidth
        thumbNailCard.position = CGPoint(x: x, y: 0)
        contentView.addChild(thumbNailCard)
        thumbNailCard.updateIfDownloading()
    }
    
    func getThumbNailSize(thumbNailImageSize:CGSize) -> CGSize {
        return CGSize(width: thumbNailImageSize.width + 2 * Thumbnail.ImagePadding, height: thumbNailImageSize.height + Thumbnail.InfoSectionHeight + Thumbnail.ImagePadding * 2)
    }
    
    fileprivate func addScrollView(_ thumbNailSize: CGSize, _ contentSize: CGSize) {
        let scrollView = UIScrollView.init(frame: view.frame)
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.contentSize = contentSize
        scrollView.canCancelContentTouches = false
        scrollView.delegate = self
        scrollView.decelerationRate = UIScrollViewDecelerationRateFast
        scrollView.backgroundColor = UIColor.clear
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(scrollViewDidTap(sender:)))
        scrollView.addGestureRecognizer(tapGesture)
        scrollView.layer.zPosition = 997
        thumbnailsScrollView = scrollView
        view.addSubview(scrollView)
        adjustOffsetWithSafeArea()
    }
    
    @objc func orientationChanged() {
        adjustOffsetWithSafeArea()
    }
    
    fileprivate func adjustOffsetWithSafeArea() {
        contentView.position.x = -thumbnailsScrollView.contentOffset.x
        var additionalOffset:CGFloat = 0
        if #available(iOS 11.0, *) {
            thumbnailsScrollView.contentSize.width = contentSize.width - view.safeAreaInsets.left * 1.2
            if UIDevice.current.orientation == .landscapeRight {
                additionalOffset = view.safeAreaInsets.right
            } else {
                additionalOffset = (view.safeAreaInsets.left > 0 ? 12 : 0)
            }
        }
        contentView.position.x = contentView.position.x - additionalOffset
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        adjustOffsetWithSafeArea()
    }
    
    @objc func scrollViewDidTap(sender:UIGestureRecognizer) {
        if sender.state == .ended {
            let touchLocation = sender.location(in: thumbnailsScene.view)
            let touchLocationConverted = thumbnailsScene.convertPoint(fromView: touchLocation)
            thumbnailsScene.touched(point: touchLocationConverted)
        }
    }
}

