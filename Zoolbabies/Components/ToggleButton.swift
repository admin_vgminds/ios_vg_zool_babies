//
//  ToggleButton.swift
//  Kids Phonics
//
//  Created by Vikram Rao on 11/01/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation
import SpriteKit

class ToggleButton: SKSpriteNode {
    
    var onImageName:String!
    var offImageName:String!
    var checked:Bool = true
    var knob:SKSpriteNode!
    var onClick:((_ checked:Bool)->Void)!
    
    let knobPadding:CGFloat = 1
    
    fileprivate func adjustKnobPosition() {
        var x:CGFloat = size.width * Metrics.half - knob.size.width * Metrics.half
        x = checked ? x - knobPadding : -x + knobPadding
        knob.position = CGPoint(x:x, y:0)
    }
    
    convenience init(onImageNamed:String, offImageNamed:String, knobImage:String, size:CGSize) {
        self.init(texture: SKTexture(imageNamed:onImageNamed), size: size)
        onImageName = onImageNamed
        offImageName = offImageNamed
        let knobWidth:CGFloat = size.height - knobPadding
        knob = SKSpriteNode(texture: SKTexture(imageNamed:knobImage), size:CGSize(width:knobWidth, height:knobWidth))
        adjustKnobPosition()
        addChild(knob)
        onClick = { checked in
            print("Set something for toggle button click!")
        }
    }
    
    fileprivate func updateUI() {
        let imageName = checked ? onImageName : offImageName
        texture = SKTexture(imageNamed:imageName!)
        adjustKnobPosition()
    }
    
    func toggle() {
        AudioPlayer.playSound("ButtonClick")
        setChecked(!checked)
        onClick(checked)
    }
    
    func setChecked(_ checked:Bool) {
        self.checked = checked
        updateUI()
    }
    
}
