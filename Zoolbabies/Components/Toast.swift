//
//  Toast.swift
//  Kids Phonics
//
//  Created by Vikram Rao on 22/01/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit

class Toast {
    
    static let fontSize = 17 * Metrics.factor
    static let fontName = "ProximaNova-Semibold"
    static var messageWindow:SKShapeNode? = nil
    
    fileprivate static func addMessage(_ message: String, _ messageWindow: SKShapeNode) {
        let label = SKLabelNode(text: message)
        label.position = CGPoint(x: 0, y: 0)
        label.horizontalAlignmentMode = .center
        label.verticalAlignmentMode = .center
        label.fontColor = UIColor.white
        label.fontName = fontName
        label.fontSize = fontSize
        messageWindow.addChild(label)
    }
    
    fileprivate static func showAnimatedlyAndHide(_ view: SKView, _ messageWindow: SKShapeNode) {
        let action = SKAction.moveTo(y: view.bounds.height - 50 * Metrics.factor, duration: 1.0)
        action.timingMode = .easeInEaseOut
        messageWindow.run(action)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+7, execute: {
            messageWindow.removeFromParent()
        })
    }
    
    static func show(message:String, scene:SKScene?) {
        guard let scene = scene else {
            return
        }
        guard let view = scene.view else {
            return
        }
        if let oldMessageWindow = self.messageWindow {
            oldMessageWindow.removeFromParent()
        }
        let size = calculateSize(message, viewSize:view.bounds.size)
        let messageWindow = addMessageWindow(size, viewFrame:view.bounds)
        addMessage(message, messageWindow)
        scene.addChild(messageWindow)
        showAnimatedlyAndHide(view, messageWindow)
        self.messageWindow = messageWindow
    }
    
    static func addMessageWindow(_ size:CGSize, viewFrame:CGRect) -> SKShapeNode {
        let messageWindow = SKShapeNode(rectOf: size, cornerRadius: 3)
        messageWindow.lineWidth = 1
        messageWindow.strokeColor = UIColor.black
        messageWindow.fillColor = UIColor.black
        messageWindow.addGlow(size: size, color: UIColor.black)
        messageWindow.position = CGPoint(x:viewFrame.midX, y:viewFrame.height + 35 * Metrics.factor)
        return messageWindow
    }
    
    static func calculateSize(_ message:String, viewSize:CGSize) -> CGSize {
        let stringRect = message.boundingRect(with: viewSize, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedStringKey.font: UIFont(name: fontName, size: fontSize)!], context: nil)
        let size = CGSize(width: stringRect.size.width + CGFloat(2) * CGFloat(10) * Metrics.factor, height: 35 * Metrics.factor)
        return size
    }
}
