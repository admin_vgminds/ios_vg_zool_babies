//
//  ImageButton.swift
//  HindiRhymes
//
//  Created by VGMINDS on 2/20/19.
//  Copyright © 2019 Vikram Rao. All rights reserved.
//

import Foundation
import SpriteKit

class ImageButton: SKSpriteNode {
    
    private var clickAction:(()->Void)?
    private var buttonName:String?
    private var radius:CGFloat?
    
    var animateClick:Bool = false
    var onBeforeClickAnimation:(()->Void)?
    
    convenience init(imageName:String, size:CGSize, animate:Bool = false) {
        self.init(color: UIColor.clear, size: size)
        self.texture = SKTexture(image: UIImage(named:imageName)!)
        if animate {
            animateWobble()
        }
    }
    
    convenience init(backgroundColor:UIColor, size:CGSize, radius:CGFloat = 0) {
        self.init(color: backgroundColor, size: size);
        self.radius = radius
        addDefaultClickHandler()
    }
    
    func onClick(_ clicked:@escaping (()->Void)) {
        self.clickAction = clicked
    }
    
    func clicked() {
        guard let clickAction = self.clickAction else {
            return
        }
        AudioPlayer.playSound("ButtonClick")
        if animateClick {
            if let onBeforeClickAnimation = self.onBeforeClickAnimation {
                onBeforeClickAnimation()
            }
            squishAnimate {
                clickAction()
            }
            return
        }
        clickAction()
    }
    
    func setImage(image: UIImage) {
        self.texture = SKTexture(image: image)
    }
    
    fileprivate func addDefaultClickHandler() {
        clickAction = { () in
            print("Set something for ImageButton click!")
        }
    }
}
