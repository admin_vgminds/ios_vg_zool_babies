//
//  NavBar.swift
//  Kids Phonics
//
//  Created by Vikram Rao on 26/02/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation
import SpriteKit

class NavBar: SKSpriteNode {
    
    var popup : PopUp!
    var view : SKView!
    
    convenience init(view:SKView!) {
        self.init(color: UIColor(rgbColorCodeRed: 0, green: 133, blue: 182, alpha: 1.0), size: CGSize(width: view.bounds.size.width, height: Metrics.navBarHeight));
        self.view = view
        //Create navigation bar
        self.position = CGPoint(x: view.bounds.midX, y: view.bounds.size.height - Metrics.navBarHeight/2)
        
        //Navbar Icons
        addIcon(foregroundImage: "cart-icon", positionFromRight: 1, animate: false) {
//            self.guardPopup {
                AudioPlayer.playSound("NoAds")
                self.popup = CartPopup(view: self.view!)
                self.popup.present(scene: self.scene!)
//            }
        }
        addIcon(foregroundImage: "settings-icon", positionFromRight: 2, animate: false) {
//            self.guardPopup {
                self.showSettings()
//            }
        }
        addIcon(foregroundImage: "rate-icon", positionFromRight: 3, animate: true) {
//            self.guardPopup {
                self.popup = RatingPopup(view: self.view!)
                self.popup.present(scene: self.scene!)
//            }
        }
    }
    
    func guardPopup(onSuccess:@escaping (()->Void)) {
        popup = AccessPopup(view: view)
        (popup as! AccessPopup).onSuccess = onSuccess
        popup.present(scene: self.scene!)
    }
    
    
    func addIcon(foregroundImage:String, positionFromRight:Int, animate:Bool, click:@escaping (()->Void)) {
        let iconHeight:CGFloat = max(35, 35 * (Metrics.factor * Metrics.half))
        let hIconPadding:CGFloat = max(20, 20 * (Metrics.factor * Metrics.half))
        
        let icon = Button(foregroundImage: foregroundImage, backgroundImage: "nav-button-bg", size: CGSize(width: iconHeight, height: iconHeight), animate: animate)
        
        var x = (view?.bounds.size.width)! * Metrics.half - hIconPadding - iconHeight * Metrics.half
        x = x - CGFloat(positionFromRight - 1) * CGFloat(iconHeight + hIconPadding)
        icon.position = CGPoint(x: x, y: 0)
        icon.animateClick = true
        icon.onClick { (name) in
            click()
        }
        
        self.addChild(icon)
    }
    
    func showSettings() {
        let popup = SettingsPopup(view: view!)
        
        popup.eraseVideoClick = {
            self.popup = EraseVideoConfirmPopup(view: self.view!)
            self.popup.present(scene: self.scene!)
        }
        
        popup.present(scene: self.scene!)
        self.popup = popup
    }
    
}
