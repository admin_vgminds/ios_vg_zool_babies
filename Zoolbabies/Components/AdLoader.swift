//
//  AdLoader.swift
//  Kids Phonics
//
//  Created by Vikram Rao on 27/02/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation
import Appodeal
import SuperAwesome

class AdLoader : NSObject, AppodealInterstitialDelegate {
    private let SUPER_AWESOME_VIDEO_ID = 37056
    private let SUPER_AWESOME_INTERSTITIAL_ID = 37054
    private var viewController:UIViewController!
    private var delegate:AdLoaderDelegate!
    private var SAInterstitialAdLoading = false
    private var SAVideoAdLoading = false
    
    fileprivate func loadSAInterstitialAd() {
        if SAInterstitialAdLoading {
            return
        }
        SAInterstitialAdLoading = true
        SAInterstitialAd.load(self.SUPER_AWESOME_INTERSTITIAL_ID)
    }
    
    fileprivate func loadSAVideoAd() {
        if SAVideoAdLoading {
            return
        }
        SAVideoAdLoading = true
        SAVideoAd.load(self.SUPER_AWESOME_VIDEO_ID)
    }
    
    convenience init(viewController:UIViewController, delegate:AdLoaderDelegate) {
        self.init()
        self.delegate = delegate
        self.viewController = viewController
        Appodeal.setInterstitialDelegate(self)
        SAInterstitialAd.setConfigurationProduction()
        #if DEBUG
        SAInterstitialAd.disableTestMode()
        #endif
        SAInterstitialAd.setCallback { (placementId, event) in
            switch event {
            case .adFailedToLoad:
                self.SAInterstitialAdLoading = false
            case .adEmpty:
                self.SAInterstitialAdLoading = false
            case .adAlreadyLoaded:
                self.SAInterstitialAdLoading = false
            case .adLoaded:
                self.SAInterstitialAdLoading = false
            case .adClosed:
                self.delegate.adLoaderFinished(success:true)
                self.loadSAInterstitialAd()
            case .adFailedToShow:
                Appodeal.showAd(AppodealShowStyle.interstitial, rootViewController: viewController)
            default:
                print("SuperAwesome Event - \(event)")
            }
        }
        loadSAInterstitialAd()
        SAVideoAd.setConfigurationProduction()
        #if DEBUG
        SAVideoAd.disableTestMode()
        #endif
        SAVideoAd.enableCloseButton()
        SAVideoAd.setCallback { (placementID, event) in
            switch event {
            case .adFailedToLoad:
                self.SAVideoAdLoading = false
            case .adEmpty:
                self.SAVideoAdLoading = false
            case .adAlreadyLoaded:
                self.SAVideoAdLoading = false
            case .adLoaded:
                self.SAVideoAdLoading = false
            case .adClosed:
                self.delegate.adLoaderFinished(success:true)
                self.loadSAVideoAd()
            case .adFailedToShow:
                Appodeal.showAd(AppodealShowStyle.interstitial, rootViewController: viewController)
            default:
                print("SuperAwesome Event: \(event)")
            }
        }
        loadSAVideoAd()
    }
    
    func load() {
        if SAVideoAd.hasAdAvailable(SUPER_AWESOME_VIDEO_ID){
            SAVideoAd.play(SUPER_AWESOME_VIDEO_ID, fromVC: self.viewController)
        } else if SAInterstitialAd.hasAdAvailable(SUPER_AWESOME_INTERSTITIAL_ID) {
            SAInterstitialAd.play(SUPER_AWESOME_INTERSTITIAL_ID, fromVC: self.viewController)
            loadSAVideoAd()
        } else {
            loadSAInterstitialAd()
            loadSAVideoAd()
            if !Appodeal.showAd(AppodealShowStyle.interstitial, rootViewController: self.viewController) {
                delegate.adLoaderFinished(success:false)
            }
        }
    }
    
    //Appodeal Interstitial Delegate Methods
    func interstitialDidLoadAdisPrecache(_ precache: Bool){
        print("Appodeal:Interstitial is precached")
    }
    
    func interstitialDidFailToLoadAd(){
        print("Appodeal:Interstitial failed to load")
        self.delegate.adLoaderFinished(success:false)
    }
    
    func interstitialWillPresent(){
        print("Appodeal:Interstitial will present the ad")
    }
    
    func interstitialDidDismiss(){
        print("Appodeal:Interstitial was closed")
        delegate.adLoaderFinished(success:true)
    }
    
    func interstitialDidClick(){
        print("Appodeal:Interstitial was clicked")
    }
    
    func interstitialDidFailToPresent() {
        print("Appodeal:Interstitial failed to present")
        delegate.adLoaderFinished(success:false)
    }
    
    func interstitialDidLoadAd() {
        print("Appodeal:Interstitial loaded")
    }
}

protocol AdLoaderDelegate {
    
    func adLoaderFinished(success:Bool)
    
}
