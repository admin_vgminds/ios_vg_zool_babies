//
//  PurchaseRestorer.swift
//  Kids Phonics
//
//  Created by Vikram Rao on 18/03/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation
import StoreKit

class PurchasesRetorer: NSObject, SKRequestDelegate {
    
    var delegate:PurchasesRetorerDelegate?
    var task:URLSessionDataTask!
    var restoreLock = NSLock()
    
    func restore() {
        if !restoreLock.try() {
            print("Restore in progress")
            return
        }
        print("Restore started...")
        let receiptRefreshRequest = SKReceiptRefreshRequest()
        receiptRefreshRequest.delegate = self
        receiptRefreshRequest.start()
    }
    
    func validateReceipt(receiptValidationUrl:URL, wrongValidationUrlCb:@escaping (()->Void)) {
        guard let receiptUrl = Bundle.main.appStoreReceiptURL else {
            print("Restore Purchase: Store receipt url not found")
            self.failed()
            return
        }
        do {
            let receiptData = try Data(contentsOf: receiptUrl)
            let encodedReceiptData = receiptData.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
            let params = ["receipt-data":encodedReceiptData]
            let requestData = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions(rawValue: 0))
            var request = URLRequest(url: receiptValidationUrl)
            request.httpMethod = "post"
            request.cachePolicy = .reloadIgnoringCacheData
            request.httpBody = requestData
            let config = URLSessionConfiguration.default
            config.requestCachePolicy = .reloadIgnoringCacheData
            let session = URLSession(configuration: config)
            task = session.dataTask(with: request) { (data, response, error) in
                guard error == nil else {
                    print("Restore Purchase: \(error!)")
                    self.failed()
                    return
                }
                guard let data = data else {
                    print("Restore Purchase: Server response is nil")
                    self.failed()
                    return
                }
                do {
                    print("Restore Purchase: Data Length - \(data.count)")
                    let dataAsString = String(data: data, encoding: String.Encoding.utf8)
                    if dataAsString != nil {
                        print("Restore Purchase: Could not convert data to string")
                        print(dataAsString!)
                    }
                    let responseJsonRaw = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions(rawValue: UInt(0)))
                    let responseJson = responseJsonRaw as! Dictionary<String, Any>
                    let status = responseJson["status"] as! Int
                    if status == 0 {
                        let receipt = responseJson["receipt"] as! Dictionary<String, Any>
                        print(receipt)
                        let inAppPurchases = receipt["in_app"] as! [Any]
                        DispatchQueue.main.async {
                            self.restoreLock.unlock()
                            if inAppPurchases.count > 0 {
                                self.delegate?.restoredPurchases()
                            } else {
                                self.delegate?.noPurchasesToRestore()
                            }
                        }
                    } else if status == 21007 {
                        print("Restore Purchase: Not the correct recipt URL")
                        wrongValidationUrlCb()
                    } else {
                        print("Restore Purchase: Server error - status - \(status)")
                        self.failed()
                    }
                } catch {
                    print("Restore Purchase: \(error)")
                    self.failed()
                }
            }
            task.resume()
        } catch {
            print("Restore Purchase: \(error)")
            self.failed()
        }
    }
    
    func requestDidFinish(_ request: SKRequest) {
        let SANDBOX_RECEIPT_URL = "https://sandbox.itunes.apple.com/verifyReceipt"
        let APPSTORE_RECEIPT_URL = "https://buy.itunes.apple.com/verifyReceipt"
        validateReceipt(receiptValidationUrl:URL(string:APPSTORE_RECEIPT_URL)!) {
            self.validateReceipt(receiptValidationUrl:URL(string:SANDBOX_RECEIPT_URL)!) {
                self.failed()
            }
        }
    }
    
    func failed() {
        DispatchQueue.main.async {
            self.restoreLock.unlock()
            self.delegate?.restorePurchasesFailed()
        }
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        print("Restore Purchase: SKReceiptRefreshRequest call failed")
        failed()
    }
    
}

protocol PurchasesRetorerDelegate {
    
    func restoredPurchases();
    
    func noPurchasesToRestore();
    
    func restorePurchasesFailed();
    
}
