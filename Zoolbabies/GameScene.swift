//
//  GameScene.swift
//  Kids App
//
//  Created by Vikram Rao on 02/01/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import SpriteKit
import GameplayKit

protocol ThumbnailCardDelegate: NSObjectProtocol {
    func thumbNailCardDidTap(videoIndex:Int)
}

class GameScene: SKScene {

    private var popup:PopUp!
    
    weak var thumbNailDelegate:ThumbnailCardDelegate!
    var thumbnailsScroller:ThumbnailsScroller!
    var navBar : NavBar!
    
    override func didMove(to view: SKView) {
        
        let backgroundNode = SKSpriteNode(texture: SKTexture(image: UIImage(named:"home-background")!), size: view.bounds.size)
        backgroundNode.position = CGPoint(x: view.bounds.midX, y: view.bounds.midY)
        addChild(backgroundNode)
        
        //Some graphics in background
        let circleSize = CGSize(width:142.5 * Metrics.factor,height:134.5 * Metrics.factor)
        let circles = SKSpriteNode(texture: SKTexture(imageNamed: "circles"), size: circleSize)
        circles.position = CGPoint(x:circleSize.width * Metrics.half,y:view.bounds.size.height - circleSize.height * Metrics.half)
        addChild(circles)
        
        let navBar = NavBar(view: view);
        addChild(navBar)
        self.navBar = navBar
        
        thumbnailsScroller = ThumbnailsScroller()
        thumbnailsScroller.make(thumbNailDelegate: thumbNailDelegate, scene: self)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("Touched (non thumbnail)")
        let touch = touches.first
        let positionInScene = touch?.location(in: self)
        touched(point: positionInScene!)
    }
    
    func findPopup() -> PopUp? {
        var popupFound : PopUp? = nil
        if let popup = self.popup {
            popupFound = popup
        } else if let popup = self.navBar.popup {
            popupFound = popup
        }
        return popupFound
    }
    
    fileprivate func removePopup() {
        let popupFound = findPopup()
        if let popup = popupFound {
            popup.remove()
        }
    }
    
    func touched(point:CGPoint) {
        let touchedNodes = self.nodes(at: point)
        for touchedNode in touchedNodes {
            if let name = touchedNode.name {
                if name == "popup-close" {
                    removePopup()
                    break
                } else if name == "popup-background" {
                    if touchedNodes.index(of: touchedNode) == 0 {
                        removePopup()
                    }
                    break
                } else if name == "restore-purchase" {
                    let cartPopup = findPopup() as! CartPopup
                    cartPopup.restorePurchases()
                }
            } else if touchedNode is Tab {
                (touchedNode as! Tab).clicked(point: self.convert(point, to: touchedNode))
                break
            } else if touchedNode is ToggleButton {
                (touchedNode as! ToggleButton).toggle()
                break
            } else if touchedNode is Button {
                (touchedNode as! Button).clicked()
                break
            } else if touchedNode is ImageButton {
                (touchedNode as! ImageButton).clicked()
            } else if touchedNode is ThumbnailCard {
                let card = touchedNode as! ThumbnailCard
                let video = DataLoader.sharedInstance.videos[card.videoIndex]
                if !(video["downloaded"] as! Bool) {
                    card.downloadFile(url: video["VideoUrl"] as! String)
                } else {
                    thumbNailDelegate.thumbNailCardDidTap(videoIndex: card.videoIndex)
                }
                break
            }
        }
    }
}

