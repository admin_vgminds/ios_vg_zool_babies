//
//  SettingsPopup.swift
//  Kids Phonics
//
//  Created by Vikram Rao on 01/02/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit

class SettingsPopup: PopUp {
    
    var eraseVideoClick:(()->Void)!
    var settingsSection:SKSpriteNode!
    var othersSection:SKSpriteNode!
    var crossPromo:ImageButton!
    var otherAppsIndex = 0
    var otherAppsTimer:Timer!
    let otherAppRollInterval = 10
    let settingsSize = CGSize(width:193 * Metrics.factor, height:220 * Metrics.factor)
    let othersSize = CGSize(width:303 * Metrics.factor, height:220 * Metrics.factor)
    var crossPromoAppUrl:String = ""
    
    init(view:SKView) {
        super.init(view: view, bgColor: UIColor.white, size: settingsSize, title: "Settings")
        contentArea.position = CGPoint(x:contentArea.position.x, y:contentArea.position.y - 40 * Metrics.factor)
        
        let tabSize = CGSize(width:230 * Metrics.factor, height:22.5 * Metrics.factor)
        let tab = Tab(size: tabSize) {tabIndex in
            if tabIndex == 0 {
                self.settingsSection.isHidden = false
                self.othersSection.isHidden = true
                self.title.text = "Settings"
                self.changeBackgroundSize(size: self.settingsSize)
                if self.otherAppsTimer != nil {
                    self.otherAppsTimer.invalidate()
                }
            } else {
                self.settingsSection.isHidden = true
                self.othersSection.isHidden = false
                self.title.text = "Others"
                self.changeBackgroundSize(size: self.othersSize)
                self.rollOtherApps()
            }
        }
        let halfPopupContentTopMargin:CGFloat = settingsSize.height * Metrics.half
        tab.position = CGPoint(x: 0, y: halfPopupContentTopMargin + 15 * Metrics.factor + 22.5 * Metrics.factor * Metrics.half)
        contentArea.addChild(tab)
        
        let pointer = SKSpriteNode(texture: SKTexture(imageNamed: "Pop-Up-pointing-Icon"), size: CGSize(width:Settings.pointerWidth, height:Settings.pointerHeight))
        pointer.position = CGPoint(x: 0, y: halfPopupContentTopMargin + Settings.pointerHeight * Metrics.half)
        contentArea.addChild(pointer)
        
        addSettingsSection()
        addOthersSection()
    }
    
    //Settings methods
    fileprivate func addSettingsSection() {
        let halfPopupContentTopMargin:CGFloat = size.height * Metrics.half
        
        let settingsSection = SKSpriteNode(color: UIColor.clear, size: size)
        settingsSection.position = CGPoint.zero
        contentArea.addChild(settingsSection)
        
        let settingsOptionSectionY:CGFloat = halfPopupContentTopMargin - 16 * Metrics.factor - 17 * Metrics.factor * Metrics.half
        
        addToggleButton("Music", settingsOptionSectionY, 1, settingsSection, UserDefaults.standard.bool(forKey: "settings-music")) { checked in
            UserDefaults.standard.set(checked, forKey: "settings-music")
            if !checked {
                AudioPlayer.stop()
            } else {
                AudioPlayer.playMusic("BGMusic")
            }
        }
        addToggleButton("Sounds", settingsOptionSectionY, 2, settingsSection, UserDefaults.standard.bool(forKey: "settings-sounds")) { checked in
            UserDefaults.standard.set(checked, forKey: "settings-sounds")
        }
        addToggleButton("Autoplay", settingsOptionSectionY, 3, settingsSection, UserDefaults.standard.bool(forKey: "settings-autoplay")) { checked in
            UserDefaults.standard.set(checked, forKey: "settings-autoplay")
        }
        
        let eraseButton = Button(imageNamed:"erase-button-bg", size:CGSize(width:Settings.eraseButtonWidth, height:Settings.eraseButtonHeight), name:"Erase all Videos", fontName:"ProximaNova-Regular", fontColor:UIColor.white, fontSize:14 * Metrics.factor)
        eraseButton.position = CGPoint(x:0, y: -halfPopupContentTopMargin + Settings.eraseButtonBottomSpacing + Settings.eraseButtonHeight * Metrics.half)
        eraseButton.onClick {name in
            self.remove(animate: false)
            self.eraseVideoClick()
        }
        
        settingsSection.addChild(eraseButton)
        settingsSection.addChild(createVersionText(parentSize: size))
        self.settingsSection = settingsSection
    }
    
    fileprivate func addToggleButton(_ name: String, _ relativeY: CGFloat, _ row: CGFloat, _ contentArea: SKSpriteNode, _ checked:Bool, action:@escaping ((_ checked:Bool)->Void)) {
        let optionLabel = SKLabelNode(text: name)
        optionLabel.verticalAlignmentMode = .center
        optionLabel.horizontalAlignmentMode = .left
        optionLabel.fontName = "ProximaNova-Regular"
        optionLabel.fontColor = UIColor(rgbColorCodeRed: 1, green: 0, blue: 0, alpha: 1.0)
        let fontSize = 17 * Metrics.factor
        optionLabel.fontSize = fontSize
        optionLabel.position = CGPoint(x: -contentArea.size.width * Metrics.half + 30.5 * Metrics.factor, y: relativeY - fontSize * row + fontSize * Metrics.half - 25 * Metrics.factor * row)
        contentArea.addChild(optionLabel)
        
        let toggleButton = ToggleButton(onImageNamed: "switch-on", offImageNamed: "switch-off", knobImage: "switch-head", size: CGSize(width:51.5 * Metrics.factor, height:20 * Metrics.factor))
        toggleButton.position = CGPoint(x: contentArea.size.width * Metrics.half - 30.5 * Metrics.factor - 51.5 * Metrics.factor * Metrics.half, y:optionLabel.position.y)
        toggleButton.onClick = action
        toggleButton.setChecked(checked)
        contentArea.addChild(toggleButton)
    }
    
    fileprivate func createVersionText(parentSize: CGSize) -> SKLabelNode {
        let version = "v" + (Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String) + " (" + (Bundle.main.infoDictionary!["CFBundleVersion"] as! String) + ")"
        let versionText = SKLabelNode(text: version)
        versionText.fontName = "ProximaNova-Regular"
        versionText.fontColor = UIColor(rgbColorCodeRed: 1, green: 0, blue: 0, alpha: 1.0)
        versionText.verticalAlignmentMode = .bottom
        versionText.horizontalAlignmentMode = .right
        let fontSize = 14 * Metrics.factor
        let size = contentArea.size
        versionText.fontSize = fontSize
        let margin = 4 * Metrics.factor
        let x:CGFloat = size.width * Metrics.half - margin
        let y:CGFloat = -size.height * Metrics.half + margin
        versionText.position = CGPoint(x:x, y:y)
        return versionText
    }
    
    //Others methods
    func addOthersSection() {
        let othersSection = SKSpriteNode(color: UIColor.clear, size: othersSize)
        othersSection.position = CGPoint.zero
        othersSection.isHidden = true
        contentArea.addChild(othersSection)
        self.othersSection = othersSection
        
        let buttonSize = CGSize(width:92 * Metrics.factor, height:26 * Metrics.factor)
        let buttonSectionTopMargin:CGFloat = 55.5 * Metrics.factor
        let buttonVerticalPadding:CGFloat = 10 * Metrics.factor
        let cornerRadius:CGFloat = 5 * Metrics.factor
        let buttonRightPadding:CGFloat = 39.5 * Metrics.factor
        let buttonX:CGFloat = self.contentArea.size.width * Metrics.half - buttonRightPadding
        var buttonY:CGFloat = self.contentArea.size.height * Metrics.half - buttonSectionTopMargin - buttonSize.height * Metrics.half
        
        let likeUsButton = Button(backgroundColor: UIColor.white, color: UIColor(rgbColorCodeRed: 95, green: 105, blue: 236, alpha: 1), size: buttonSize, name: "Like Us", fontName: "ProximaNova-Regular", fontColor: UIColor.white, fontSize: 13 * Metrics.factor, radius: cornerRadius)
        likeUsButton.position = CGPoint(x:buttonX, y:buttonY)
        likeUsButton.onClick { (name) in
            UIApplication.shared.openURL(URL(string: "https://www.facebook.com/Videogyan")!)
        }
        othersSection.addChild(likeUsButton)
        
        let aboutUsButton = Button(backgroundColor: UIColor.white, color: UIColor(rgbColorCodeRed: 88, green: 184, blue: 55, alpha: 1), size: buttonSize, name: "About Us", fontName: "ProximaNova-Regular", fontColor: UIColor.white, fontSize: 13 * Metrics.factor, radius: cornerRadius)
        buttonY -= (buttonSize.height + buttonVerticalPadding)
        aboutUsButton.position = CGPoint(x:buttonX, y:buttonY)
        aboutUsButton.onClick { (name) in
            UIApplication.shared.openURL(URL(string: "http://videogyan.com")!)
        }
        othersSection.addChild(aboutUsButton)
        
        let watchUsButton = Button(backgroundColor: UIColor.white, color: UIColor(rgbColorCodeRed: 94, green: 211, blue: 182, alpha: 1), size: buttonSize, name: "Watch Us", fontName: "ProximaNova-Regular", fontColor: UIColor.white, fontSize: 13 * Metrics.factor, radius: cornerRadius)
        buttonY -= (buttonSize.height + buttonVerticalPadding)
        watchUsButton.position = CGPoint(x:buttonX, y:buttonY)
        watchUsButton.onClick { (name) in
            UIApplication.shared.openURL(URL(string: "https://www.youtube.com/user/videogyan")!)
        }
        othersSection.addChild(watchUsButton)
        
        let faqsButton = Button(backgroundColor: UIColor.white, color: UIColor(rgbColorCodeRed: 248, green: 178, blue: 56, alpha: 1), size: buttonSize, name: "FAQ's", fontName: "ProximaNova-Regular", fontColor: UIColor.white, fontSize: 13 * Metrics.factor, radius: cornerRadius)
        buttonY -= (buttonSize.height + buttonVerticalPadding)
        faqsButton.position = CGPoint(x:buttonX, y:buttonY)
        faqsButton.onClick { (name) in
            UIApplication.shared.openURL(URL(string: "http://www.kidzooly.com/faq_rhymes.html")!)
        }
        othersSection.addChild(faqsButton)
        
        let otherAppsSize = CGSize(width:115 * Metrics.factor, height:116.5 * Metrics.factor)
        let otherAppsLeftPadding = 10.5 * Metrics.factor
        let otherAppsTopPadding = 63.5 * Metrics.factor
        let otherAppsImage = ImageButton(backgroundColor: UIColor(rgbColorCodeRed: 234, green: 234, blue: 234, alpha: 1), size: otherAppsSize)
        otherAppsImage.position = CGPoint(x:-othersSize.width * Metrics.half + otherAppsSize.width * Metrics.half + otherAppsLeftPadding, y:othersSize.height * Metrics.half - otherAppsSize.height * Metrics.half - otherAppsTopPadding)
        otherAppsImage.onClick {
            self.crossPromoImageClicked()
        }
        othersSection.addChild(otherAppsImage)
        crossPromo = otherAppsImage
    }
    
    func rollOtherApps() {
        otherAppsIndex = 0
        showOtherApp()
        otherAppsTimer = Timer.scheduledTimer(timeInterval: TimeInterval(otherAppRollInterval), target: self, selector: #selector(showOtherApp), userInfo: nil, repeats: true)
    }
    
    fileprivate func incrementNextOtherAppIndex() {
        let otherApps = DataLoader.sharedInstance.otherAppsInfo["OtherApps"]! as! [Dictionary<String, AnyObject>]
        if otherApps.count == 1 {
            otherAppsTimer.invalidate()
            return
        }
        otherAppsIndex = otherAppsIndex == otherApps.count - 1 ? 0 : otherAppsIndex + 1
        return
    }
    
    @objc func showOtherApp() {
        let otherApps = DataLoader.sharedInstance.otherAppsInfo["OtherApps"]! as! [Dictionary<String, AnyObject>]
        let otherAppsInfo = otherApps[otherAppsIndex]
        let thumbnailUrl = otherAppsInfo["ThumbnailLink"] as! String
        crossPromoAppUrl = otherAppsInfo["AppStoreLink"] as! String
        
        let thumbNailExists = AssetManager.exists(httpUrl: thumbnailUrl)
        if !thumbNailExists {
            incrementNextOtherAppIndex()
            return
        }
        if let thumbNailUrl = AssetManager.localUrl(httpUrl: thumbnailUrl) {
            if let thumbNailImage = UIImage(contentsOfFile: thumbNailUrl.path) {
                crossPromo.setImage(image: thumbNailImage)
                crossPromo.squishAnimate {
                    
                }
            }
        }
        incrementNextOtherAppIndex()
    }
    
    func crossPromoImageClicked() {
        if !crossPromoAppUrl.isEmpty {
            UIApplication.shared.openURL(URL(string: crossPromoAppUrl)!)
            print("Cross promo image button clicked. Url: \"" + crossPromoAppUrl + "\"")
        }
    }
}
