//
//  CartPopup.swift
//  Kids Phonics
//
//  Created by Vikram Rao on 22/01/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit
import StoreKit

class CartPopup:PopUp,ProductsManagerDelegate,PurchasesRetorerDelegate {
    
    let productsManager = ProductsManager()
    var buyButton:Button!
    var priceLabel:SKLabelNode!
    var rebateText1:SKLabelNode!
    var rebateInd:SKSpriteNode!
    var purchasesRestorer = PurchasesRetorer()
    var restoreButton:SKLabelNode!
    var transactionLock = NSLock()
    var subContentArea:SKSpriteNode!
    
    fileprivate func addContent(_ size: CGSize) {
        let buyAreaSize = CGSize(width:226 * Metrics.factor,height:58 * Metrics.factor)
        let buyAreaBottomPadding = 43.5 * Metrics.factor
        let buyAreaBg = SKSpriteNode(color: UIColor(rgbColorCodeRed: 235, green: 234, blue: 234, alpha: 1), size: buyAreaSize)
        buyAreaBg.position = CGPoint(x:5 * Metrics.factor,y:-size.height * Metrics.half + buyAreaSize.height * Metrics.half + buyAreaBottomPadding)
        contentArea.addChild(buyAreaBg)
        
        let subContentArea = SKSpriteNode(color: UIColor.clear, size: size)
        subContentArea.position = CGPoint.zero
        contentArea.addChild(subContentArea)
        self.subContentArea = subContentArea
        
        let noAdsLabelWidth:CGFloat = 47 * Metrics.factor
        let noAdsLabelLeftPadding:CGFloat = 99 * Metrics.factor
        let noAdsLabelBottomPadding:CGFloat = 74.5 * Metrics.factor
        let noAdsLabelHeight:CGFloat = 10.5 * Metrics.factor
        let noAdsLabel = SKLabelNode(text: "No Ads")
        noAdsLabel.verticalAlignmentMode = .center
        noAdsLabel.horizontalAlignmentMode = .center
        noAdsLabel.fontName = "ProximaNova-Regular"
        noAdsLabel.fontSize = 15 * Metrics.factor
        noAdsLabel.fontColor = UIColor.black
        noAdsLabel.position = CGPoint(x:-size.width * Metrics.half + noAdsLabelLeftPadding + noAdsLabelWidth * Metrics.half,y:-size.height * Metrics.half + noAdsLabelBottomPadding +  noAdsLabelHeight * Metrics.half)
        subContentArea.addChild(noAdsLabel)
        
        let priceLabel = SKLabelNode(text: "...")
        priceLabel.verticalAlignmentMode = .center
        priceLabel.horizontalAlignmentMode = .center
        priceLabel.fontName = "ProximaNova-Semibold"
        priceLabel.fontSize = 15 * Metrics.factor
        priceLabel.fontColor = UIColor(rgbColorCodeRed: 36, green: 226, blue: 32, alpha: 1.0)
        let priceLabelLeftPadding:CGFloat = 99 * Metrics.factor
        let priceLabelWidth:CGFloat = 42 * Metrics.factor
        let priceLabelBottomPadding:CGFloat = 56.5 * Metrics.factor
        let priceLabelHeight:CGFloat = 13 * Metrics.factor
        priceLabel.position = CGPoint(x:-size.width * Metrics.half + priceLabelLeftPadding + priceLabelWidth * Metrics.half, y:-size.height * Metrics.half + priceLabelBottomPadding + priceLabelHeight * Metrics.half)
        subContentArea.addChild(priceLabel)
        self.priceLabel = priceLabel
        
        let noAdsImageLeftPadding = 49.5 * Metrics.factor
        let noAdsImageBottomPadding = 54.5 * Metrics.factor
        let noAdsImageWidth = 34 * Metrics.factor
        let noAdsImageHeight = 34 * Metrics.factor
        let noAdsImage = SKSpriteNode(texture: SKTexture(imageNamed: "no-ads"), size: CGSize(width:34 * Metrics.factor, height:34 * Metrics.factor))
        noAdsImage.position = CGPoint(x:-size.width * Metrics.half + noAdsImageLeftPadding + noAdsImageWidth * Metrics.half,y:-size.height * Metrics.half + noAdsImageBottomPadding + noAdsImageHeight * Metrics.half)
        subContentArea.addChild(noAdsImage)
        
        let rebateInd = SKSpriteNode(texture: SKTexture(imageNamed:"discount-bg"), size:CGSize(width:23*Metrics.factor,height:23*Metrics.factor))
        rebateInd.position = CGPoint(x:-size.width * Metrics.half + 24.5 * Metrics.factor + 23*Metrics.factor * Metrics.half,y:-size.height * Metrics.half + 87.5 * Metrics.factor + 23 * Metrics.factor * Metrics.half)
        let rebateText1 = SKLabelNode(text:"50%")
        rebateText1.verticalAlignmentMode = .center
        rebateText1.horizontalAlignmentMode = .center
        rebateText1.fontName = "ProximaNova-Semibold"
        rebateText1.fontSize = 7.3 * Metrics.factor
        rebateText1.fontColor = UIColor.white
        rebateText1.position = CGPoint(x:0, y:4*Metrics.factor)
        rebateInd.addChild(rebateText1)
        self.rebateText1 = rebateText1
        
        let rebateText2 = SKLabelNode(text:"Off")
        rebateText2.verticalAlignmentMode = .center
        rebateText2.horizontalAlignmentMode = .center
        rebateText2.fontName = "ProximaNova-Semibold"
        rebateText2.fontSize = 7.3 * Metrics.factor
        rebateText2.fontColor = UIColor.white
        rebateText2.position = CGPoint(x:0, y:-4*Metrics.factor)
        rebateInd.addChild(rebateText2)
        
        rebateInd.isHidden = true
        subContentArea.addChild(rebateInd)
        self.rebateInd = rebateInd
        var alreadyPurchasedText:String
        
        let buyButtonSize = CGSize(width:83 * Metrics.factor,height:27 * Metrics.factor)
        if !UserDefaults.standard.bool(forKey: "no-ads-purchased") {
            buyButton = Button(imageNamed:"buy-btn-bg", size:buyButtonSize, name:"Check Price", fontName:"ProximaNova-Regular", fontColor:UIColor.white, fontSize:14 * Metrics.factor)
            buyButton.onClick {name in
                self.buy()
            }
            downloadProducts()
            
            let restorePurchaseButton:SKLabelNode;
            if #available(iOS 11, *) {
                let attrs = [
                    NSAttributedStringKey.font : UIFont(name: "ProximaNova-Regular", size: 13 * Metrics.factor)!,
                    NSAttributedStringKey.foregroundColor : UIColor.blue,
                    NSAttributedStringKey.underlineStyle : 1] as [NSAttributedStringKey : Any]
                let btnName = NSMutableAttributedString(string:"Restore Purchase", attributes:attrs)
                restorePurchaseButton = SKLabelNode(attributedText:btnName)
            } else {
                restorePurchaseButton = SKLabelNode(text:"Restore Purchase")
                restorePurchaseButton.fontName = "ProximaNova-Regular"
                restorePurchaseButton.fontSize = 13 * Metrics.factor
                restorePurchaseButton.fontColor = UIColor.blue
            }
            restorePurchaseButton.name = "restore-purchase"
            restorePurchaseButton.verticalAlignmentMode = .center
            restorePurchaseButton.horizontalAlignmentMode = .center
            let restoreButtonLeftPadding = 12 * Metrics.factor
            let restoreButtonWidth = 10.5 * Metrics.factor
            restorePurchaseButton.position = CGPoint(x:0, y:-size.height * Metrics.half + restoreButtonLeftPadding + restoreButtonWidth * Metrics.half)
            subContentArea.addChild(restorePurchaseButton)
            self.restoreButton = restorePurchaseButton
            alreadyPurchasedText = "Already Purchased?"
        } else {
            let noAdsLabelLeftPadding1:CGFloat = 99 * Metrics.factor
            let noAdsLabelWidth1:CGFloat = 47 * Metrics.factor
            let noAdsLabelBottomPadding1:CGFloat = 67.5 * Metrics.factor
            let noAdsLabelHeight1:CGFloat = 10.5 * Metrics.factor
            priceLabel.isHidden = true
            noAdsLabel.position = CGPoint(x:-size.width * Metrics.half + noAdsLabelLeftPadding1 + noAdsLabelWidth1 * Metrics.half,y:-size.height * Metrics.half + noAdsLabelBottomPadding1 + noAdsLabelHeight1 * Metrics.half)
            buyButton = Button(imageNamed:"green-half-button-bg", size:buyButtonSize, name:"Purchased", fontName:"ProximaNova-Regular", fontColor:UIColor.white, fontSize:16 * Metrics.factor)
            buyButton.onClick {name in
                
            }
            alreadyPurchasedText = "You are enjoying ad free experience"
        }
        let buyButtonRightPadding:CGFloat = 34 * Metrics.factor
        let xvalue = size.width * Metrics.half - buyButtonRightPadding - buyButtonSize.width * Metrics.half
        let yvalue = -size.height * Metrics.half + 59.5 * Metrics.factor + buyButtonSize.height * Metrics.half
        buyButton.position = CGPoint(x: xvalue, y: yvalue)
        subContentArea.addChild(buyButton)
        
        let alreadyPurchasedLabel = SKLabelNode(text: alreadyPurchasedText)
        alreadyPurchasedLabel.verticalAlignmentMode = .center
        alreadyPurchasedLabel.horizontalAlignmentMode = .center
        alreadyPurchasedLabel.fontName = "ProximaNova-Regular"
        alreadyPurchasedLabel.fontSize = 10 * Metrics.factor
        alreadyPurchasedLabel.fontColor = UIColor.black
        alreadyPurchasedLabel.position = CGPoint(x:0, y:-size.height * Metrics.half + 27 * Metrics.factor + 10.5 * Metrics.factor * Metrics.half)
        subContentArea.addChild(alreadyPurchasedLabel)
    }
    
    init(view: SKView) {
        let size = CGSize(width:283 * Metrics.factor, height:234.5 * Metrics.factor)
        super.init(view: view, bgColor: UIColor.clear, size: size, title: "Premium Pack", titleColor:UIColor.white)
        productsManager.delegate = self
        purchasesRestorer.delegate = self
        
        contentArea.texture = SKTexture(imageNamed:"cart-bg")
        
        addContent(size)
    }
    
    func downloadProducts() {
        if !Reachability.isConnectedToNetwork() {
            Toast.show(message:"No Internet Connection", scene:self.scene)
            return
        }
        buyButton.setTitle("Wait...")
        productsManager.downloadProducts()
    }
    
    fileprivate func getProduct(_ products: [SKProduct], _ productIdentifierToUse: String) -> SKProduct? {
        for product in products {
            if product.productIdentifier == productIdentifierToUse {
                return product
            }
        }
        return nil
    }
    
    func getInAppProductFor(key:String, products:[SKProduct]) -> SKProduct? {
        let identifier = DataLoader.sharedInstance.adsInfo.keys.contains(key) ? DataLoader.sharedInstance.adsInfo[key] : nil
        if let id = identifier {
            return getProduct(products, id as! String)
        }
        return nil
    }
    
    func getInt(value:Any?, defaultValue:Int) -> Int {
        if value == nil {
            return defaultValue
        } else if value is Int {
            return value as! Int
        } else if value is String {
            return Int(value as! String)!
        }
        return defaultValue
    }
    
    func isDiscountDay() -> Bool {
        let interval = getInt(value:DataLoader.sharedInstance.adsInfo["DiscountInterval"], defaultValue:2)
        return Calendar.current.component(.day, from: Date()) % interval == 1
    }
    
    func getProductToUse(products:[SKProduct]) -> SKProduct? {
        return isDiscountDay() ? getInAppProductFor(key: "DiscountedNoAdsInAppId", products: products) : getInAppProductFor(key: "NoAdsInAppId", products: products)
    }
    
    func getDiscount(_ products:[SKProduct]) -> Double {
        let discountedProduct = getInAppProductFor(key: "DiscountedNoAdsInAppId", products: products)
        let regularProduct = getInAppProductFor(key: "NoAdsInAppId", products: products)
        var discount:Double = 0
        if (discountedProduct != nil && regularProduct != nil) {
            discount = round(((regularProduct?.price.doubleValue)! - (discountedProduct?.price.doubleValue)!) / (regularProduct?.price.doubleValue)! * 100)
        } else {
            print("ERROR! Discounted product id not found")
        }
        return discount
    }
    
    func didDownloadProducts(_ products:[SKProduct]) {
        if let product = getProductToUse(products: products) {
            buyButton.setTitle("Buy Now")
            if isDiscountDay() {
                let discount = getDiscount(products)
                self.rebateInd.isHidden = false
                self.rebateText1.text = "\(Int(discount))%"
                self.rebateInd.squishAnimate {
                    
                }
            }
            priceLabel.text = formattedPrice(product)
        }
    }
    
    func didBuyProduct() {
        dismissable = true
        transactionLock.unlock()
        UserDefaults.standard.set(true, forKey: "no-ads-purchased")
        Toast.show(message: "Successful. Enjoy ad free experience!", scene: self.scene)
        rerender()
    }
    
    func didBuyProductFailed() {
        dismissable = true
        transactionLock.unlock()
        buyButton.setTitle("Buy Now")
        Toast.show(message: "Oops! Something went wrong. Try again!", scene: self.scene)
    }
    
    func formattedPrice(_ inAppProduct:SKProduct) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.formatterBehavior = .behavior10_4
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = inAppProduct.priceLocale
        let price1Str = numberFormatter.string(from: inAppProduct.price)
        return price1Str!
    }
    
    func didDownloadProductsFailed() {
        Toast.show(message: "Oops! Something went wrong. Try again!", scene: self.scene)
        buyButton.setTitle("Check Price")
    }
   
    func buy() {
        if !Reachability.isConnectedToNetwork() {
            Toast.show(message:"No Internet Connection", scene:self.scene)
            AudioPlayer.playSound("NoInternetConnection")
            return
        }
        if !transactionLock.try() {
            print("Transaction in progress")
            return
        }
        if productsManager.products == nil || productsManager.products?.count == 0 {
            downloadProducts()
            return
        }
        buyButton.setTitle("Buying..")
        dismissable = false
        if let product = getProductToUse(products: productsManager.products!) {
            productsManager.buy(product)
        }
    }
    
    override func remove(animate:Bool = true, onComplete:(()->Void)? = nil) {
        if dismissable {
            productsManager.delegate = nil
            purchasesRestorer.delegate = nil
        }
        super.remove(animate: animate, onComplete: onComplete)
    }
    
    func restorePurchasesFailed() {
        dismissable = true
        transactionLock.unlock()
        setRestoreButtonText("Restore Purchase")
        Toast.show(message: "Could not restore purchases", scene: self.scene)
    }
    
    fileprivate func rerender() {
        self.subContentArea.removeAllChildren()
        self.addContent(self.subContentArea.size)
    }
    
    func restoredPurchases() {
        dismissable = true
        transactionLock.unlock()
        UserDefaults.standard.set(true, forKey: "no-ads-purchased")
        Toast.show(message: "Purchase restored. Enjoy ad free experience!", scene: self.scene)
        rerender()
    }
    
    func noPurchasesToRestore() {
        dismissable = true
        transactionLock.unlock()
        setRestoreButtonText("Restore Purchase")
        Toast.show(message: "No previous purchases found", scene: self.scene)
    }
    
    fileprivate func setRestoreButtonText(_ text: String) {
        if #available(iOS 11, *) {
            let attrs = [
                NSAttributedStringKey.font : UIFont(name: "ProximaNova-Regular", size: 13 * Metrics.factor)!,
                NSAttributedStringKey.foregroundColor : UIColor.black,
                NSAttributedStringKey.underlineStyle : 1] as [NSAttributedStringKey : Any]
            let btnName = NSMutableAttributedString(string:text, attributes:attrs)
            restoreButton.attributedText = btnName
        } else {
            restoreButton.text = text
        }
    }
    
    func restorePurchases() {
        if !Reachability.isConnectedToNetwork() {
            Toast.show(message:"No Internet Connection", scene:self.scene)
            AudioPlayer.playSound("NoInternetConnection")
            return
        }
        if !transactionLock.try() {
            print("Transaction in progress")
            return
        }
        dismissable = false
        setRestoreButtonText("Restoring...")
        purchasesRestorer.restore()
    }
}




