//
//  RatingPopup.swift
//  Kids Phonics
//
//  Created by Vikram Rao on 26/02/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import Foundation
import SpriteKit

class RatingPopup: PopUp {
    
    init(view:SKView) {
        let size = CGSize(width:220 * Metrics.factor, height:220 * Metrics.factor)
        super.init(view: view, bgColor: UIColor.white, size: size, title: "Happy with our App?", headerImageName:"rating-header-image")
        
        let halfPopupContentTopMargin:CGFloat = size.height * Metrics.half
        
//        let starWidth:CGFloat = 191 * Metrics.factor
//        let starHeight:CGFloat = 29 * Metrics.factor
        
//        let stars = SKSpriteNode(texture: SKTexture(imageNamed: "5-stars"), color: UIColor.white, size: CGSize(width:starWidth, height:starHeight))
//        stars.position = CGPoint(x:0, y:10 * Metrics.factor)
//        contentArea.addChild(stars)
        
        let rateUsButton = Button(imageNamed:"erase-button-bg", size:CGSize(width:Settings.eraseButtonWidth, height:Settings.eraseButtonHeight), name:"Rate Us", fontName:"ProximaNova-Regular", fontColor:UIColor.white, fontSize:14 * Metrics.factor)
        rateUsButton.position =  CGPoint(x:0, y:10 * Metrics.factor)
        rateUsButton.onClick {name in
            self.remove()
            let appStoreId = Bundle.main.object(forInfoDictionaryKey: "app-store-id") as! String
            let appStoreReviewUrl = "itms-apps://itunes.apple.com/app/id\(appStoreId)?action=write-review"
            UIApplication.shared.openURL(URL(string: appStoreReviewUrl)!)
        }
        contentArea.addChild(rateUsButton)
        
        let remindMeLaterButton = Button(imageNamed:"remind-me-later-bg", size:CGSize(width:Settings.eraseButtonWidth, height:Settings.eraseButtonHeight), name:"Remind me later", fontName:"ProximaNova-Regular", fontColor:UIColor(rgbColorCodeRed:86, green:86, blue:86, alpha:1.0), fontSize:14 * Metrics.factor)
        remindMeLaterButton.position = CGPoint(x:0, y: -halfPopupContentTopMargin + Settings.eraseButtonBottomSpacing * CGFloat(1.5) +  Settings.eraseButtonHeight * CGFloat(1.5))
        remindMeLaterButton.onClick {name in
            self.remove()
        }
        contentArea.addChild(remindMeLaterButton)
    }
    
}
