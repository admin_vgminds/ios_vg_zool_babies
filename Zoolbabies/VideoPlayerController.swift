//
//  VideoPlayerController.swift
//  Kids App
//
//  Created by Vikram Rao on 04/01/18.
//  Copyright © 2018 Vikram Rao. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class AVPlayerView: UIView {
    override class var layerClass: AnyClass {
        return AVPlayerLayer.self
    }
}

class VideoPlayerController: UIViewController, AdLoaderDelegate {
    
    private let interstitialInterval = 3
    
    var videos:[Dictionary<String,Any>]!
    var downloadedVideos:[Dictionary<String,Any>]!
    var videoIndex:Int!
    
    private var player:AVPlayer!
    private var videoInfo:Dictionary<String,Any>!
    private var time:CMTime!
    private var paused:Bool = true
    private var initializing = false
    private var autoResume = true
    private var hideControlsTimer:Timer!
    private var upNextTimer:Timer!
    private var seekerUpdaterTimer:Timer!
    private var delayedResumeTimer:Timer!
    private var upNextWidthOriginal:CGFloat!
    private var videosPlayedCount = 1
    private var videoAdjusted:Bool = false
    private var adLoader:AdLoader!
    
    @IBOutlet weak var prevButtonLeftMargin: NSLayoutConstraint!
    @IBOutlet weak var nextButtonRightMargin: NSLayoutConstraint!
    @IBOutlet weak var seeker: UISlider!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var videoPlayerContainer: UIView!
    @IBOutlet weak var controlsContainer: UIView!
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var prevButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var videoPrevButton: UIButton!
    @IBOutlet weak var videoNextButton: UIButton!
    @IBOutlet weak var upNextPlayButton: UIButton!
    @IBOutlet weak var upNextVideoContainer: UIView!
    @IBOutlet weak var upNextVideoPlayerContainer: UIImageView!
    @IBOutlet weak var videoPlayerBottomSpacing: NSLayoutConstraint!
    @IBOutlet weak var upNextTopMargin: NSLayoutConstraint!
    @IBOutlet weak var upNextWidth: NSLayoutConstraint!
    @IBOutlet weak var upNextRightMargin: NSLayoutConstraint!
    @IBOutlet weak var upNextLabel: UILabel!
    @IBOutlet weak var completedTimer: UILabel!
    @IBOutlet weak var pendingTimer: UILabel!
    @IBOutlet weak var controlsBottomSpacing: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.isIdleTimerDisabled = true
        var downloadedVideos:[Dictionary<String,Any>] = []
        var delta = 0
        for pos in 0..<videos.count {
            var video = videos[pos]
            if (video["downloaded"] as! Bool) {
                video["original-index"] = pos
                downloadedVideos.append(video)
            } else {
                if videoIndex > pos {
                    delta = delta + 1
                }
            }
        }
        videoIndex = videoIndex - delta
        self.downloadedVideos = downloadedVideos
        setupVideoUI()
        addVideoPlayerActions()
        adjustButtonInsets()
        seeker.setThumbImage(UIImage(named:"video-slider-head"), for: .normal)
        self.controlsContainer.isHidden = true
        self.controlsContainer.alpha = 0.0
        hideControls(animated: false)
        self.adLoader = AdLoader(viewController: self, delegate: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if autoResume {
            resume()
            showControls()
            autoDownloadVideo()
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if upNextWidthOriginal == nil {
            upNextWidthOriginal = self.upNextWidth.constant
        }
        if !videoAdjusted {
            adjustForSafeArea()
            videoAdjusted = true
        }
    }
    
    fileprivate func adjustForSafeArea() {
        if #available(iOS 11.0, *) {
            if UIDevice.current.orientation == .landscapeLeft {
                prevButtonLeftMargin.constant = 9 + view.safeAreaInsets.left * 0.5
                nextButtonRightMargin.constant = 11
            } else {
                prevButtonLeftMargin.constant = 9
                nextButtonRightMargin.constant = 11 + view.safeAreaInsets.right * 0.5
            }
        }
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        adjustForSafeArea()
        super.willRotate(to: toInterfaceOrientation, duration: duration)
    }
    
    fileprivate func hideUpNext() {
        upNextVideoContainer.alpha = 0.0
    }
    
    fileprivate func setupVideoUI() {
        initData()
        resetTime()
        setPlayButtonImage()
        createVideoPlayer()
    }
    
    fileprivate func adjustButtonInsets() {
        prevButton.imageEdgeInsets = UIEdgeInsetsMake(6, 12, 6, 12)
        nextButton.imageEdgeInsets = UIEdgeInsetsMake(6, 12, 6, 12)
        homeButton.imageEdgeInsets = UIEdgeInsetsMake(7, 7, 11, 7)
        upNextPlayButton.imageEdgeInsets = UIEdgeInsetsMake(7, 8, 7, 7)
        videoNextButton.imageEdgeInsets = UIEdgeInsetsMake(13, 10, 13, 10)
        videoPrevButton.imageEdgeInsets = UIEdgeInsetsMake(13, 10, 13, 10)
    }
    
    fileprivate func addVideoTapGesture() {
        let videoTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(toggleControls))
        videoPlayerContainer.addGestureRecognizer(videoTapGestureRecognizer)
        let upNextVideoTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(nextVideo(_:)))
        upNextVideoContainer.addGestureRecognizer(upNextVideoTapGestureRecognizer)
    }
    
    fileprivate func listenWhenAppGoesToBackground() {
        NotificationCenter.default.addObserver(self, selector:#selector(pause), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
    }
    
    fileprivate func listenWhenVideoFinishesPlaying() {
        NotificationCenter.default.addObserver(self, selector:#selector(playerDidFinishPlaying),name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
    }
    
    fileprivate func initData() {
        videoInfo = downloadedVideos[videoIndex]
    }
    
    fileprivate func createVideoPlayer(videoUrl:String) -> AVPlayer? {
        guard let url = AssetManager.localUrl(httpUrl: videoUrl) else {
            return nil
        }
        let player = AVPlayer(url: url)
        let playerLayer = videoPlayerContainer.layer as! AVPlayerLayer
        playerLayer.player = player
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspect
        return player
    }
    
    func getThumbnail(url:String) -> UIImage {
        let sourceURL = URL(string: url)
        let asset = AVAsset(url: sourceURL!)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        let time = CMTimeMake(1, 1)
        let imageRef = try! imageGenerator.copyCGImage(at: time, actualTime: nil)
        let thumbnail = UIImage(cgImage:imageRef)
        return thumbnail
    }
    
    fileprivate func createVideoPlayer() {
        let videoUrl = videoInfo["VideoUrl"] as! String
        if let player = self.player {
            if let currentItem = player.currentItem {
                NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: currentItem)
            }
        }
        player = createVideoPlayer(videoUrl: videoUrl)
        listenWhenVideoFinishesPlaying()
        
    }
    
    fileprivate func addVideoPlayerActions() {
        listenWhenAppGoesToBackground()
        addVideoTapGesture()
    }
    
    @objc fileprivate func updateSeeker() {
        var totalDuration = (self.player.currentItem?.duration.seconds)!
        totalDuration = totalDuration.isNaN ? 0 : totalDuration
        let elapsed = self.player.currentTime().seconds
        seeker.value = Float(elapsed / totalDuration)
        pendingTimer.text = (totalDuration - elapsed).timeFormat()
        completedTimer.text = elapsed.timeFormat()
    }
    
    fileprivate func seekToSavedTime(complete:(()->Void)? = nil) {
        initializing = true
        if player.status == .readyToPlay && player.currentItem?.status == .readyToPlay {
            player.seek(to: time, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero, completionHandler: { (finished:Bool) in
                self.initializing = false
                self.updateSeeker()
                if complete != nil {
                    complete!()
                }
            })
        } else {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: {
                self.seekToSavedTime(complete: complete)
            })
        }
    }
    
    fileprivate func stopSeekerUpdateTimer() {
        if (self.seekerUpdaterTimer != nil && self.seekerUpdaterTimer.isValid) {
            self.seekerUpdaterTimer.invalidate()
        }
    }
    
    func play() {
        if paused {
            return
        }
        self.player.play()
        self.stopSeekerUpdateTimer()
        self.seekerUpdaterTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.updateSeeker), userInfo: nil, repeats: true)
    }
    
    fileprivate func setupUpNextVideo() {
        let pendingTime = Int((self.player.currentItem?.asset.duration.seconds)!) - Int((self.player.currentItem?.currentTime().seconds)!)
        let videoLayer = self.videoPlayerContainer.layer as! AVPlayerLayer
        self.upNextTopMargin.constant = (videoLayer.videoRect.size.height - self.videoPlayerContainer.bounds.height) * 0.5 - (40 * Metrics.factor)
        self.upNextRightMargin.constant = (videoLayer.videoRect.size.width - self.videoPlayerContainer.bounds.width) * 0.5 - (8 * Metrics.factor)
        print("Top margin for up next - \(self.upNextTopMargin.constant)")
        self.upNextWidth.constant = self.upNextWidthOriginal * Metrics.factor
        if self.upNextTimer != nil && self.upNextTimer.isValid {
            self.upNextTimer.invalidate()
        }
        let nextVideoIndex = self.videoIndex == self.downloadedVideos.count - 1 ? 0 : self.videoIndex + 1
        let nextVideoInfo = self.downloadedVideos[nextVideoIndex]
        
        let originalIndexForThumbnail = nextVideoInfo["original-index"] as! Int
        if let upNextImage = UIImage(named: "anim_\(originalIndexForThumbnail+1)_1") {
            self.upNextVideoPlayerContainer.image = upNextImage
        } else if let url = AssetManager.localUrl(httpUrl: (nextVideoInfo["ThumbnailUrl"] as! String)) {
            let data = try? Data(contentsOf: url)
            self.upNextVideoPlayerContainer.image = UIImage(data: data!)!
        }
        
        var upNextDelay = pendingTime - 10
        upNextDelay = upNextDelay <= 0 ? 1 : upNextDelay
        self.upNextTimer = Timer.scheduledTimer(timeInterval: TimeInterval(upNextDelay), target: self, selector: #selector(self.showUpNextVideo), userInfo: nil, repeats: false)
    }
    
    fileprivate func seekToSavedTimeAndPlay() {
        seekToSavedTime {
            self.play()
            self.setupUpNextVideo()
        }
    }
    
    @objc fileprivate func showUpNextVideo() {
        UIView.animate(withDuration: 0.5) {
            self.upNextVideoContainer.alpha = 1.0
        }
    }
    
    fileprivate func resetTime() {
        time = kCMTimeZero
    }
    
    @objc func playerDidFinishPlaying() {
        if UserDefaults.standard.bool(forKey: "settings-autoplay") {
            nextVideo(self)
        } else {
            pause()
            resetTime()
            seekToSavedTime()
        }
    }
    
    @objc func resume() {
        if initializing {
            return
        }
        stopDelayedResumeTimer()
        paused = false
        setPlayButtonImage()
        seekToSavedTimeAndPlay()
        hideControlsAfterDelay()
    }
    
    @objc func pause() {
        paused = true
        player.pause()
        stopDelayedResumeTimer()
        stopSeekerUpdateTimer()
        time = player.currentTime()
        setPlayButtonImage()
        showControls()
    }
    
    fileprivate func setPlayButtonImage() {
        let imageName = (paused ? "video-play" : "video-pause")
        playButton.setImage(UIImage(named:imageName), for: .normal)
        var left:CGFloat = 7, right:CGFloat = 7
        if paused {
            left = 9
            right = 5
        }
        playButton.imageEdgeInsets = UIEdgeInsetsMake(7, left, 7, right)
    }
    
    fileprivate func hideControlsAfterDelay() {
        stopHideControlsTimer()
        hideControlsTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(hideControls), userInfo: nil, repeats: false)
    }
    
    @objc func toggleControls() {
        if controlsContainer.isHidden {
            showControls()
        } else {
            hideControls()
        }
    }
    
    @objc func hideControls(animated:Bool = true) {
        if controlsContainer.isHidden {
            self.hideUpNext()
            return
        }
        if !animated {
            self.controlsContainer.alpha = 0.0
            self.hideUpNext()
            self.controlsContainer.isHidden = true
            return
        }
        UIView.animate(withDuration: 0.5, animations: {
            self.controlsContainer.alpha = 0.0
            self.hideUpNext()
        }, completion: { (completed:Bool) in
            if completed {
                self.controlsContainer.isHidden = true
            }
        })
    }
    
    @objc fileprivate func showControls() {
        if !controlsContainer.isHidden {
            return
        }
        showUpNextVideo()
        controlsContainer.alpha = 0.0
        controlsContainer.isHidden = false
        UIView.animate(withDuration: 0.5) {
            self.controlsContainer.alpha = 1.0
            self.hideControlsAfterDelay()
        }
    }
    
    @objc fileprivate func togglePlay() {
        stopHideControlsTimer()
        paused ? resume() : pause()
    }
    
    @IBAction func pauseClicked(sender:Any) {
        AudioPlayer.playSound("ButtonClick")
        togglePlay()
    }
    
    @IBAction func seekToStart(sender:Any) {
        AudioPlayer.playSound("ButtonClick")
        player.pause()
        resetTime()
        resume()
    }
    
    @IBAction func seekBackSomeTime(sender:Any) {
        AudioPlayer.playSound("ButtonClick")
        player.pause()
        stopHideControlsTimer()
        var targetTimeInSeconds = player.currentTime().seconds - 5
        targetTimeInSeconds = targetTimeInSeconds < 0 ? 0 : targetTimeInSeconds
        setTime(targetTimeInSeconds)
        resume()
    }
    
    fileprivate func setTime(_ targetTimeInSeconds: Double) {
        time = CMTime(seconds: targetTimeInSeconds, preferredTimescale: player.currentTime().timescale)
    }
    
    fileprivate func stopHideControlsTimer() {
        if (hideControlsTimer != nil) && hideControlsTimer.isValid {
            hideControlsTimer.invalidate()
        }
    }
    
    @IBAction func seekForwardSomeTime(sender:Any) {
        AudioPlayer.playSound("ButtonClick")
        player.pause()
        stopHideControlsTimer()
        var targetTimeInSeconds = player.currentTime().seconds + 5
        let maxTime = (player.currentItem?.asset.duration.seconds)!
        targetTimeInSeconds = targetTimeInSeconds > maxTime ? maxTime : targetTimeInSeconds
        setTime(targetTimeInSeconds)
        resume()
    }
    
    @IBAction func seekToEndTime(sender:Any) {
        AudioPlayer.playSound("ButtonClick")
        player.pause()
        time = player.currentItem?.asset.duration
        resume()
    }
    
    fileprivate func stopDelayedResumeTimer() {
        if (delayedResumeTimer != nil && delayedResumeTimer.isValid) {
            delayedResumeTimer.invalidate()
        }
    }
    
    @IBAction func seekVideo(_ sender: Any) {
        if !paused {
            pause()
        }
        stopHideControlsTimer()
        let targetTimeInSeconds = Double(seeker.value) * (self.player.currentItem?.duration.seconds)!
        setTime(targetTimeInSeconds)
        stopDelayedResumeTimer()
        delayedResumeTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(resume), userInfo: nil, repeats: false)
    }
    
    fileprivate func reloadVideoAndPlay() {
        pause()
        setupVideoUI()
        resume()
    }
    
    fileprivate func showInterstitialReloadVideoAndPlay() {
        self.videosPlayedCount += 1
        if self.videosPlayedCount % interstitialInterval == 0 && Reachability.isConnectedToNetwork() && !UserDefaults.standard.bool(forKey: "no-ads-purchased") {
            pause()
            self.autoResume = false
            self.adLoader.load()
            return
        }
        reloadVideoAndPlay()
        
    }
    
    @IBAction func previousVideo(_ sender: Any) {
        AudioPlayer.playSound("ButtonClick")
        videoIndex = videoIndex > 0 ? videoIndex - 1 : downloadedVideos.count - 1
        autoDownloadVideo(downloadIfFirstVideo: true)
        showInterstitialReloadVideoAndPlay()
    }
    
    fileprivate func autoDownloadVideo(downloadIfFirstVideo:Bool = false) {
        if (videoIndex == 0 && downloadIfFirstVideo) || videoIndex == downloadedVideos.count - 1 {
            AutoVideoDownloader.sharedInstance.next({ (percent, videoId) in
                if percent == 100 {
                    DataLoader.sharedInstance.videos[videoId]["downloaded"] = true
                    self.videos[videoId]["original-index"] = videoId
                    self.downloadedVideos.append(self.videos[videoId])
                    self.setupUpNextVideo()
                }
            })
        }
    }
    
    @IBAction func nextVideo(_ sender: Any) {
        AudioPlayer.playSound("ButtonClick")
        videoIndex = videoIndex == downloadedVideos.count - 1 ? 0 : videoIndex + 1
        self.autoDownloadVideo()
        self.showInterstitialReloadVideoAndPlay()
        
        
    }
    
    @IBAction func home(_ sender: Any) {
        AudioPlayer.playSound("ButtonClick")
        pause();
        videoPlayerContainer.layer.removeFromSuperlayer()
        navigationController?.popViewController(animated: true)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            AudioPlayer.playMusic("BGMusic")
        }
    }
    
    func adLoaderFinished(success:Bool) {
        if success {
            if controlsContainer.alpha == 0 {
                controlsContainer.isHidden = true
            }
        }
            print("adLoaderFinished -> controlsContainer.isHidden = \(controlsContainer.isHidden)")
            if !autoResume {
                reloadVideoAndPlay()
                autoResume = true
            }
        }
    }
    
    

